# Introduction
stackAlign aligns z-stacks to each other. The current framework does this by 
performing a slice-registration process, where images are parsed through a 
similarity transform (scaling, translation, rotation), and then compared to each
other by a euclidian distance metric.

Distances are normalised with respect to the lowest distance identified, and 
subsequently inverted, such that
```math
d_{i,j} = \text{euc}(\text{Image}_i, \text{Image}_j)
```
and 
```math
d^{norm}_{i,j} = 1 / d_{min}.
```

This gives all pairs of possible matches a score in the range $`(0, 1]`$, 
where the pair scoring 1 is the best identified pair.

We use these resulting scores to produce a scoring matrix we can now plug into 
our dynamic programming algorithm. We will utilise an approach where we allow 
for sequence overlaps, and gaps in the bigger stack. We will **not** tolerate gaps
in the smaller stack (except in the edges), as all of the smaller stacks should 
have a corresponding match somewhere in the larger one, as long as it's not in 
one of the edges.

![](doc/img/grid_directions.png)
![](doc/img/grid_edges.png)
![](doc/img/grid_start_finish.png)
![](doc/img/grid_long_path.png)