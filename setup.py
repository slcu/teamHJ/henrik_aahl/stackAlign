# setup.py
# Usage: ``python setup.py build_ext --inplace && python setup.py install``
#from distutils.core import setup, Extension
from setuptools import setup, Extension, find_packages
import numpy

requirements = []
setup_requirements = []
test_requirements = []

setup(
    name='stackAlign',
    author='Henrik Ahl',
    version='0.1dev',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.6',
    ],
    package_dir = {
        'stackAlign': 'stackAlign',
        'stackAlign.analyse': 'stackAlign/analyse',
        'stackAlign.external': 'stackAlign/external'
        },
    ext_modules=[Extension(
        'stackAlign.external._psf',
        ['stackAlign/external/psf.c'],
        include_dirs=['stackAlign/external',
        numpy.get_include()],
        )],
    packages=find_packages(
	include=['stackAlign',
		 'stackAlign/external',
		 'stackAlign/analyse']),
#    packages=['stackAlign', 'stackAlign.external',
#              'stackAlign.analyse'],
    license='GNU GPL v3.0',
    description='A package for processing confocal image stacks',
    long_description=open('README.md').read()
)
print numpy.get_include()


