#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 25 11:41:42 2018

@author: henrikahl
"""

import numpy as np
from scipy.ndimage.morphology import binary_dilation, binary_erosion
from itertools import cycle


def contour_fit_threshold(data, threshold=.8, smooth_iterate=3):
    contour = np.array(data < threshold * 0.5 * np.mean(data.flatten()),
                       dtype='bool')
    contour = smooth_contour(contour, smooth_iterate)  # TODO
    return contour


def step(data, contour, weighting):
    inside = contour > 0
    outside = contour <= 0

    c0 = np.sum(data[outside]) / float(np.sum(outside))
    c1 = np.sum(data[inside]) / float(np.sum(inside))

    dres = np.array(np.gradient(contour))
    abs_dres = np.abs(dres).sum(0)

    aux = abs_dres * (weighting * (data - c1)**2 - 1. /
                      float(weighting) * (data - c0)**2)
    contour[aux < 0] = 1
    contour[aux > 0] = 0
    return contour


def smooth_contour(contour, iterate=1):
    curvop = fcycle([SIoIS, ISoSI])
    for ii in xrange(iterate):
        contour = curvop(contour)
    return contour


def contour_fit(data, contour, weighting, iterate_smooth):
    iterations = np.min(np.shape(data))
    contour = set_contour_to_box()
    for ii in range(iterations):
        contour = step(weighting)
        contour = smooth_contour(iterate_smooth)
        print("Contour fit: iteration %s / %s..." % (ii + 1, iterations))
    return contour


def set_contour_to_box(data):
    contour = getplanes(np.shape(data))
    contour = setedges(contour, 0)
    contour[0] = 0
    return contour


def setedges(array, value=0):
    array[[0, 0, -1, -1], [0, -1, 0, -1], :] = value
    array[:, [0, 0, -1, -1], [0, -1, 0, -1]] = value
    array[[0, -1, 0, -1], :, [0, 0, -1, -1]] = value
    return array


def getedges(shape):
    array = np.ones(shape, dtype='bool')
    array = setedges(array)
    return array


def setplanes(array, value=0):
    array[[0, -1], :, :] = value
    array[:, [0, -1], :] = value
    array[:, :, [0, -1]] = value
    return array


def getplanes(shape, invert=True):
    if invert:
        array = np.zeros(shape, dtype='bool')
        array = setplanes(array, value=1)
    else:
        array = np.ones(shape, dtype='bool')
        array = setplanes(array, value=0)
    return array


class fcycle(object):
    def __init__(self, iterable):
        self.funcs = cycle(iterable)

    def __call__(self, *args, **kwargs):
        f = next(self.funcs)
        return f(*args, **kwargs)


def SI(u, iterate=1):
    P = np.array([np.zeros((3, 3, 3)) for i in xrange(9)])
    P[0][:, :, 1] = 1
    P[1][:, 1, :] = 1
    P[2][1, :, :] = 1
    P[3][:, [0, 1, 2], [0, 1, 2]] = 1
    P[4][:, [0, 1, 2], [2, 1, 0]] = 1
    P[5][[0, 1, 2], :, [0, 1, 2]] = 1
    P[6][[0, 1, 2], :, [2, 1, 0]] = 1
    P[7][[0, 1, 2], [0, 1, 2], :] = 1
    P[8][[0, 1, 2], [2, 1, 0], :] = 1
    _aux = np.zeros((0), dtype='bool')
    if u.shape != _aux.shape[1:]:
        _aux = np.zeros((len(P),) + u.shape, dtype='bool')
    for ii in xrange(len(P)):
        _aux[ii] = binary_erosion(u, P[ii], iterations=iterate,
                                  mask=getedges(np.shape(u)))
    return np.max(_aux, axis=0)


def IS(u, iterate=1):
    P = np.array([np.zeros((3, 3, 3), dtype='bool') for ii in xrange(9)])
    P[0][:, :, 1] = 1
    P[1][:, 1, :] = 1
    P[2][1, :, :] = 1
    P[3][:, [0, 1, 2], [0, 1, 2]] = 1
    P[4][:, [0, 1, 2], [2, 1, 0]] = 1
    P[5][[0, 1, 2], :, [0, 1, 2]] = 1
    P[6][[0, 1, 2], :, [2, 1, 0]] = 1
    P[7][[0, 1, 2], [0, 1, 2], :] = 1
    P[8][[0, 1, 2], [2, 1, 0], :] = 1
    _aux = np.zeros((0), dtype='bool')
    if u.shape != _aux.shape[1:]:
        _aux = np.zeros((len(P),) + u.shape, dtype='bool')
    for ii in xrange(len(P)):
        _aux[ii] = binary_dilation(u, P[ii], iterations=iterate,
                                   mask=getedges(np.shape(u)))
    return np.min(_aux, axis=0)


def SIoIS(u):
    return SI(IS(u))


def ISoSI(u):
    return IS(SI(u))
