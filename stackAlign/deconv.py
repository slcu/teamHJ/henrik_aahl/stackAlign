
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May  3 12:02:46 2018

@author: henrik
"""
import os
import gc
from skimage.restoration import richardson_lucy
from stackAlign.external import psf
import tifffile as tiff
import copy
import numpy as np
import scipy

def create_psf(zshape, rshape, zdims, rdims, ex_wavelen, em_wavelen, pinhole_radius,
               num_aperture=1.0, refr_index=1.333,  pinhole_shape='square',
               psf_type=(psf.ISOTROPIC | psf.CONFOCAL), magnification=20.0):

    args = dict(shape=(int(zshape), int(rshape)),
                dims=(zdims, rdims),
                ex_wavelen=ex_wavelen,
                em_wavelen=em_wavelen,
                num_aperture=num_aperture,
                refr_index=refr_index,
                pinhole_radius=pinhole_radius,
                pinhole_shape=pinhole_shape,
                magnification=magnification)

    obsvol = psf.PSF(psf_type, **args)

    return obsvol.volume()

def deconvolve(data, psf_vol, iterations, method='LR', threshold=0):
    data[data < threshold] = 0

    if method == 'LR':
#        for ii in xrange(iterations):
        data = richardson_lucy(image=data, psf=psf_vol, iterations=iterations,
                               clip=False)
        gc.collect()
    data[data < 0] = 0  
    data = scipy.signal.wiener(data, mysize=(3, 3, 3), noise=500000)
    data[data < 0] = 0

    return data

def deconvolve_file_to_file(fname, output_dir, iterations, method, num_aperture=None,
                            refr_index=None, zshape=None, rshape=None, zdims=None,
                            rdims=None, ex_wavelen=None, em_wavelen=None,
                            pinhole_radius=None, pinhole_shape='square',
                            psf_type=(psf.ISOTROPIC | psf.CONFOCAL),
                            substep=1, clip=False, magnification=20):

    # num_aperture=None
    # refr_index=None
    # zshape=None
    # rshape=None
    # zdims=None
    # rdims=None
    # ex_wavelen=None
    # em_wavelen=None
    # pinhole_radius=None
    # pinhole_shape='square'
    #psf_type=(psf.ISOTROPIC | psf.CONFOCAL)
    # substep=1
    # clip=False
    # magnification=20
    # fname = '/home/henrik/projects/stackAlign/data/pWUS-3X-VENUS-pCLV3-mCherry-on-NPA-2-0h-mCherry-0.7-Gain-800_topcrop.tif'

    # Get file
    file_ = tiff.TiffFile(fname)
    meta = file_.imagej_metadata
    data = file_.asarray()

    # TODO
    if data.shape[1] == 1:
        print "Data not in right format. Attempting to correct. Might cause errors."
        data = data[:, 0]

    # Correct or assign deconvolution data
    if zshape is None:
        zshape = data.shape[0] / 2.
    if rshape is None:
        rshape = data.shape[-1] / 2.
    if rdims is None:
        rdims = rshape * \
            np.mean(eval(meta['voxelsize'])[0] + eval(meta['voxelsize'])[1])
    if zdims is None:
        zdims = zshape * eval(meta['voxelsize'])[2]
    if ex_wavelen is None:
        ex_wavelen = eval(meta['excitation_wavelength'])[0]
    if em_wavelen is None:
        em_wavelen = np.mean(eval(meta['emission_range'])[0])
    if pinhole_radius is None:
        pinhole_radius = eval(meta['pinhole_radius'])[0] / magnification
    if num_aperture is None:
        num_aperture = meta['num_aperture']
    if refr_index is None:
        refr_index = meta['refr_index']

    # Create point-spread function based on
    psf_obj = create_psf(zshape, rshape, zdims, rdims, ex_wavelen, em_wavelen,
                         num_aperture, refr_index, pinhole_radius,
                         pinhole_shape=pinhole_shape, psf_type=psf_type,
                         magnification=magnification)

    # Deconvolve and correct array type
    deconv = deconvolve(data, psf_obj, iterations, method,
                        substep=substep, clip=clip)
    deconv = np.array(deconv, dtype=np.uint16)

    # Save
    fnamebase = os.path.splitext(os.path.basename(fname))[0]
    outname = fnamebase + '_deconv' + '.tif'
    outname = os.path.join(output_dir, outname)
    tiff.imsave(outname, deconv, imagej=True, metadata=meta)


#print('Creating PSF configuration files...')
# p.map(functools.partial(ip.generate_all_psf_configs, nChannels=3,
#                      PSF_CONFIG_PATH=PSF_CONFIG_PATH, WAVELENGTHS=WAVELENGTHS,
#                      method='BW', NA=1.0, refr_ind_immersion=1.33), sfiles) # TODO: sfiles->chfiles
#
#################################################################################
#''' Make actual PSF files '''
#print('Creating PSF files...')
#pfiles = os.listdir(PSF_CONFIG_PATH)
#pfiles = map(lambda x: os.path.join(PSF_CONFIG_PATH, x), pfiles)
# Note: Can't parallelise this due to uncustomizability of output
# map(functools.partial(ip.create_psf,
#                      PSF_PATH=PSF_PATH,
#                      PSF_CONFIG_PATH=PSF_CONFIG_PATH,
#                      PSF_GENERATOR_PATH=PSF_GENERATOR_PATH), pfiles)
#''' Perform deconvolution '''
# TODO: Move this to imageproc
# def deconvolve(fname):
#  fnamebase = os.path.basename(fname)
#  psf_file_path = os.path.join(PSF_PATH, 'PSF-BW_%s' % fnamebase) # TODO: Allow for other methods
#  mip_file_path = os.path.join('mips', os.path.splitext(fnamebase)[0] + '_mip')
#  deconv_file_path = os.path.join('deconv',
#                                  os.path.splitext(fnamebase)[0] + '_deconv')
#
#  if 'C1' in fnamebase:
#    wl = DECONV_ITERS[0]
#  elif 'C2' in fnamebase:
#    wl = DECONV_ITERS[1]
#  elif 'C3' in fnamebase:
#    wl = DECONV_ITERS[2]
#
#  subprocess.check_call(
#      ['java',       '-jar',  DECONVOLUTIONLAB_PATH, 'Run',
#       '-image',     'file',  fname,
#       '-psf',       'file',  psf_file_path,
#       '-algorithm', 'RL',    str(wl),
#       '-display',   'no',
#       '-out',       'stack', deconv_file_path,      'noshow',
#       '-out',       'mip',   mip_file_path,         'noshow',
#       '-monitor',   'console',
#       '-stats',     'no',
#       '-path',      OUTPUT_PATH
#       ])
#################################################################################
#print('Deconvolving files...')
# TODO: Apply on stretching_adjusted files
#ch1 = os.listdir(OUTPUT_PATH + '/channel1')
#ch2 = os.listdir(OUTPUT_PATH + '/channel2')
#ch3 = os.listdir(OUTPUT_PATH + '/channel3')
#ch1 = map(lambda x: os.path.join(OUTPUT_PATH, "channel1", x), ch1)
#ch2 = map(lambda x: os.path.join(OUTPUT_PATH, "channel2", x), ch2)
#ch3 = map(lambda x: os.path.join(OUTPUT_PATH, "channel3", x), ch3)
#
#all_files_channels = ch1 + ch2 + ch3
# all_files_channels.sort()
#cfiles = copy.deepcopy(all_files_channels)
#
# Filter out the ones we have already done
# all_files_channels = filter(lambda x:
# not os.path.isfile(
# os.path.join(
# OUTPUT_PATH,
# 'deconv',
# os.path.splitext(os.path.basename(x))[0] + '_deconv.tif')),
# all_files_channels)
#
#p.close(); p.join(); del p
#p = multiprocessing.Pool(NPROCESSORS)
#p.map(deconvolve, all_files_channels)
