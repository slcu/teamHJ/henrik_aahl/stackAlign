#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 30 10:53:32 2018

@author: henrik
"""


from __future__ import division, print_function
from numpy.fft import fft2, ifft2, fftshift
import numpy as np
import math
import scipy.ndimage.interpolation as ndii
from scipy import ndimage
from scipy import optimize
import tifffile as tiff
import os
import misc
import subprocess
import copy


def get_resolution_x(file_):
    '''Assumes um. '''

    if type(file_) == str:
        file_ = tiff.TiffFile(file_)

    return file_.pages.pages[0].tags['XResolution'].value[0] / 1e6


def resolution_y(file_):
    '''Assumes um. '''

    if type(file_) == str:
        file_ = tiff.TiffFile(file_)

    return file_.pages.pages[0].tags['YResolution'].value[0] / 1e6


def resolution_z(file_):
    '''Assumes um and ImageJ format. '''

    if type(file_) == str:
        file_ = tiff.TiffFile(file_)

    return file_.imagej_metadata['spacing']


def resolution_xyz(file_):
    '''Assumes um and ImageJ format. '''

    if type(file_) == str:
        file_ = tiff.TiffFile(file_)

    return np.array([file_.pages.pages[0].tags['XResolution'].value[0] / 1e6,
                     file_.pages.pages[0].tags['YResolution'].value[0] / 1e6,
                     file_.imagej_metadata['spacing']])



def topcrop(fname, output_dir, threshold=0):
    #  fnamebase = os.path.basename(fname)
    #  outfname  = os.path.join(output_dir, os.path.splitext(fnamebase)[0] + '_topcrop' + '.tif')

    outfname = misc.add_suffix(fname, "_topcrop", output_dir=output_dir)

    sfile_ = tiff.TiffFile(fname)
    data = np.array(sfile_.asarray(), dtype='uint16')
    meta = sfile_.imagej_metadata

    tot_int = np.sum(data, axis=tuple(range(1, len(data.shape))))

    start = list(tot_int > threshold).index(True)
    stop = len(tot_int) - list(tot_int[::-1] > threshold).index(True)

    data = data[start:stop]
    meta["slices"] = len(data)
    meta["images"] = len(data) * meta["channels"]

    tiff.imsave(outfname, data=data, imagej=True, metadata=meta)


def lsm_to_tiff(fname, output_dir, extra_metadata=dict()):
    fnamebase = os.path.basename(fname)
    outfname = os.path.join(
        output_dir, os.path.splitext(fnamebase)[0] + '.tif')

    sfile_ = tiff.TiffFile(fname)
    data = np.array(sfile_.asarray(), dtype='uint16')
    meta = sfile_.lsm_metadata

    nmeta = dict()
    nmeta["dim"] = 3
    nmeta["channels"] = meta['DimensionChannels']
    nmeta["dimensionx"] = meta['DimensionX']
    nmeta["dimensiony"] = meta['DimensionY']
    nmeta["dimensionz"] = meta['DimensionZ']
#  nmeta["shape"]     = (meta["DimensionX"], meta["DimensionY"], meta["DimensionZ"])
    nmeta["max"] = np.max(data)
    nmeta["mean"] = np.mean(data)
    nmeta["min"] = np.min(data)
    nmeta["origin"] = [meta["OriginX"] * 1e6,
                       meta["OriginY"] * 1e6, meta["OriginZ"] * 1e6]
    nmeta["type"] = 'uint16'
    nmeta["extent"] = [0, 0, 0]
    nmeta["voxelsize"] = meta['VoxelSizeX'] * \
        1e6, meta['VoxelSizeY'] * 1e6, meta['VoxelSizeZ'] * 1e6
    nmeta['spacing'] = meta['VoxelSizeZ'] * 1e6

    detection_channels = meta['ScanInformation']['Tracks'][0]['DetectionChannels']
    nmeta['channel_names'] = [dchan['ChannelName']
                              for dchan in detection_channels]
    nmeta['gain'] = [dchan['DetectorGainFirst']
                     for dchan in detection_channels]
    nmeta['dye'] = [dchan['DyeName'] for dchan in detection_channels]
    nmeta['pinhole_radius'] = [dchan['PinholeDiameter'] /
                               2. for dchan in detection_channels]
    nmeta['emission_range'] = [[round(dchan['SpiWavelengthStart']), round(
        dchan['SpiWavelengthStop'])] for dchan in detection_channels]

    for key in extra_metadata:
        nmeta[key] = extra_metadata[key]

    tiff.imsave(outfname, data=data[0], imagej=True, metadata=nmeta)


def get_lsm_metadata(fname):
    file_ = tiff.TiffFile(fname)
    return file_.lsm_metadata


def split_channels(fname, output_path):
    fnamebase = os.path.basename(fname)
    file_ = tiff.TiffFile(fname)
    data = file_.asarray()
    metadata = file_.imagej_metadata

    if data.ndim > 4:
        data = data[:, 0]

    # Change important metadata
    # TODO: Change all metadata values
    metadata['images'] /= metadata['channels']
    metadata['channels'] = 1

    # NOTE: matrix might not be ordered like this
    for ii in xrange(data.shape[1]):
        outpath = os.path.join(output_path,
                               'channel%d' % (ii + 1),
                               'C%d_%s' % (ii + 1, fnamebase))
        channel_metadata = copy.deepcopy(metadata)
        channel_metadata['gain'] = [eval(metadata['gain'])[ii]]
        channel_metadata['pinhole_radius'] = [
            eval(metadata['pinhole_radius'])[ii]]
        channel_metadata['emission_range'] = [
            eval(metadata['emission_range'])[ii]]
        channel_metadata['excitation_wavelength'] = [
            eval(metadata['excitation_wavelength'])[ii]]
        channel_metadata['channel_names'] = [
            eval(metadata['channel_names'])[ii]]
        channel_metadata['dye'] = [eval(metadata['dye'])[ii]]

        tiff.imsave(outpath, data=np.array(data[:, ii, ...], dtype='uint16'),
                    imagej=True, metadata=channel_metadata)


def similarity(im0, im1):
    """Return similarity transformed image im1 and transformation parameters.

    Transformation parameters are: isotropic scale factor, rotation angle (in
    degrees), and translation vector.

    A similarity transformation is an affine transformation with isotropic
    scale and without shear.

    Limitations:
    Image shapes must be equal and square.
    All image areas must have same scale, rotation, and shift.
    Scale change must be less than 1.8.
    No subpixel precision.

    """
    if im0.shape != im1.shape:
        raise ValueError("Images must have same shapes.")
    elif len(im0.shape) != 2:
        raise ValueError("Images must be 2 dimensional.")

    f0 = fftshift(abs(fft2(im0)))
    f1 = fftshift(abs(fft2(im1)))

    h = highpass(f0.shape)
    f0 *= h
    f1 *= h
    del h

    f0, log_base = logpolar(f0)
    f1, log_base = logpolar(f1)

    f0 = fft2(f0)
    f1 = fft2(f1)
    r0 = abs(f0) * abs(f1)
    ir = abs(ifft2((f0 * f1.conjugate()) / r0))
    i0, i1 = np.unravel_index(np.argmax(ir), ir.shape)
    angle = 180.0 * i0 / ir.shape[0]
    scale = log_base ** i1

    if scale > 1.8:
        ir = abs(ifft2((f1 * f0.conjugate()) / r0))
        i0, i1 = np.unravel_index(np.argmax(ir), ir.shape)
        angle = -180.0 * i0 / ir.shape[0]
        scale = 1.0 / (log_base ** i1)
        if scale > 1.8:
            print("Images are not compatible. Scale change > 1.8. Returning maximal value matrix and (0, 0 (0, 0))")
            return np.zeros(im1.shape) + np.iinfo(im1.dtype).max, 0, 0, [-0, -0]

    if angle < -90.0:
        angle += 180.0
    elif angle > 90.0:
        angle -= 180.0

    im2 = ndii.zoom(im1, 1.0 / scale)
    im2 = ndii.rotate(im2, angle)

    if im2.shape < im0.shape:
        t = np.zeros_like(im0)
        t[:im2.shape[0], :im2.shape[1]] = im2
        im2 = t
    elif im2.shape > im0.shape:
        im2 = im2[:im0.shape[0], :im0.shape[1]]

    f0 = fft2(im0)
    f1 = fft2(im2)
    ir = abs(ifft2((f0 * f1.conjugate()) / (abs(f0) * abs(f1))))
    t0, t1 = np.unravel_index(np.argmax(ir), ir.shape)

    if t0 > f0.shape[0] // 2:
        t0 -= f0.shape[0]
    if t1 > f0.shape[1] // 2:
        t1 -= f0.shape[1]

    im2 = ndii.shift(im2, [t0, t1])

    # correct parameters for ndimage's internal processing
    if angle > 0.0:
        d = int((int(im1.shape[1] / scale) * math.sin(math.radians(angle))))
        t0, t1 = t1, d + t0
    elif angle < 0.0:
        d = int((int(im1.shape[0] / scale) * math.sin(math.radians(angle))))
        t0, t1 = d + t1, d + t0
    scale = (im1.shape[1] - 1) / (int(im1.shape[1] / scale) - 1)

    return im2, scale, angle, [-t0, -t1]


def highpass(shape):
    """Return highpass filter to be multiplied with fourier transform."""
    x = np.outer(
        np.cos(np.linspace(-math.pi / 2., math.pi / 2., shape[0])),
        np.cos(np.linspace(-math.pi / 2., math.pi / 2., shape[1])))
    return (1.0 - x) * (2.0 - x)


def logpolar(image, angles=None, radii=None):
    """Return log-polar transformed image and log base."""
    shape = image.shape
    center = shape[0] / 2, shape[1] / 2
    if angles is None:
        angles = shape[0]
    if radii is None:
        radii = shape[1]
    theta = np.empty((angles, radii), dtype=np.float64)
    theta.T[:] = -np.linspace(0, np.pi, angles, endpoint=False)
    #d = radii
    d = np.hypot(shape[0] - center[0], shape[1] - center[1])
    log_base = 10.0 ** (math.log10(d) / (radii))
    radius = np.empty_like(theta)
    radius[:] = np.power(log_base, np.arange(radii, dtype=np.float64)) - 1.0
    x = radius * np.sin(theta) + center[0]
    y = radius * np.cos(theta) + center[1]
    output = np.empty_like(x)
    ndii.map_coordinates(image, [x, y], output=output)
    return output, log_base


def generate_all_psf_configs(fname, nChannels, PSF_CONFIG_PATH, WAVELENGTHS,
                             method='BW', NA=1.0, refr_ind_immersion=1.33):
    metadata = tiff.TiffFile(fname).imagej_metadata
    for jj in xrange(nChannels):
        psf_name = os.path.join(
            PSF_CONFIG_PATH,
            'PSF-BW_C%d_%s_config.txt' %
            (jj + 1, os.path.splitext(os.path.basename(fname))[0]))
        generate_PSF_config(fname=psf_name,
                            method=method,
                            NX=metadata['dimensionx'],
                            NY=metadata['dimensiony'],
                            NZ=metadata['dimensionz'],
                            NA=NA,
                            wavelength=WAVELENGTHS[jj],
                            xystep=eval(metadata['voxelsize'])[0] * 1e3,
                            zstep=eval(metadata['voxelsize'])[2] * 1e3,
                            #                           xystep=metadata['voxelsizex']*1e9,
                            #                           zstep=metadata['voxelsizez']*1e9,
                            refr_ind_immersion=refr_ind_immersion)


def create_psf(fname, PSF_PATH, PSF_CONFIG_PATH, PSF_GENERATOR_PATH):
    subprocess.check_call(["java", "-cp", PSF_GENERATOR_PATH,
                           "PSFGenerator", fname])
    os.rename("PSF BW.tif", os.path.join(PSF_PATH,
                                         os.path.basename(fname).split('_config')[0] + '.tif'))


def generate_PSF_config(fname, method, NX, NY, NZ, NA, wavelength, xystep, zstep,
                        refr_ind_immersion, accuracy="Good", LUT="Fire"):
    if method == 'RW':
        output = \
            """#PSFGenerator
#Mon Apr 16 13:03:52 BST 2018
""" + \
            "PSF-shortname=%s" % fname + \
            """
psf-Circular-Pupil-defocus=100.0
psf-Oriented-Gaussian-axial=Linear
""" + \
            "psf-RW-NI=%f\n" % refr_ind_immersion + \
            "ResLateral=%f" % xystep + \
            """
psf-Defocus-DBot=30.0
psf-Oriented-Gaussian-focus=0.0
psf-Astigmatism-axial=Linear
psf-TV-NI=1.5
""" + \
            "psf-RW-accuracy=%s" % accuracy + \
            """
psf-Cardinale-Sine-axial=Linear
psf-Lorentz-axial=Linear
psf-Gaussian-defocus=100.0
psf-Astigmatism-focus=0.0
""" + \
            "psf-BW-accuracy=%s" % accuracy + \
            """
psf-Cardinale-Sine-focus=0.0
psf-GL-ZPos=2000.0
psf-Koehler-dMid=3.0
psf-Lorentz-focus=0.0
psf-TV-TI=150.0
psf-Koehler-dTop=1.5
psf-Koehler-n1=1.0
psf-Circular-Pupil-axial=Linear
psf-Lorentz-defocus=100.0
psf-Koehler-n0=1.5
psf-Circular-Pupil-focus=0.0
psf-Koehler-dBot=6.0
psf-Defocus-ZI=2000.0
psf-VRIGL-NS2=1.4
psf-VRIGL-NS1=1.33
psf-Double-Helix-defocus=100.0
psf-TV-ZPos=2000.0
psf-Cosine-defocus=100.0
Scale=Linear
""" + \
            "psf-BW-NI=%f\n" % refr_ind_immersion + \
            "psf-GL-NS=%f\n" % refr_ind_immersion + \
            "Lambda=%f" % wavelength + \
            """
psf-GL-NI=1.5
psf-Astigmatism-defocus=100.0
""" + \
            "ResAxial=%f" % zstep + \
            """
psf-GL-TI=150.0
psf-Double-Helix-axial=Linear
""" + \
            "LUT=%s" % LUT + \
            """
psf-VRIGL-RIvary=Linear
""" + \
            "NZ=%d" % NZ + \
            """
psf-Double-Helix-focus=0.0
""" + \
            "NY=%d\n" % NY + \
            "NX=%d\n" % NX + \
            "psf-VRIGL-accuracy=%s" % accuracy + \
            """
psf-Gaussian-axial=Linear
psf-VRIGL-ZPos=2000.0
psf-Gaussian-focus=0.0
psf-Cardinale-Sine-defocus=100.0
psf-VRIGL-NI=1.5
Type=16-bits
""" + \
            "NA=%f" % NA + \
            """
psf-Oriented-Gaussian-defocus=100.0
psf-VRIGL-NG=1.5
psf-VRIGL-TI=150.0
psf-Defocus-DMid=1.0
psf-VRIGL-TG=170.0
psf-Defocus-K=275.0
psf-Cosine-axial=Linear
psf-TV-NS=1.0
psf-Cosine-focus=0.0
psf-Defocus-DTop=30.0
""" + \
            "psf-GL-accuracy=%s" % accuracy
    elif method == 'BW':
        output = \
            """#PSFGenerator
#Mon Apr 16 13:03:25 BST 2018
psf-Circular-Pupil-defocus=100.0
psf-Oriented-Gaussian-axial=Linear
psf-RW-NI=1.5
""" + \
            "ResLateral=%f" % xystep + \
            """
psf-Defocus-DBot=30.0
psf-Oriented-Gaussian-focus=0.0
psf-Astigmatism-axial=Linear
psf-TV-NI=1.5
""" + \
            "psf-RW-accuracy=%s" % accuracy + \
            """
psf-Cardinale-Sine-axial=Linear
psf-Lorentz-axial=Linear
psf-Gaussian-defocus=100.0
psf-Astigmatism-focus=0.0
""" + \
            "psf-BW-accuracy=%s" % accuracy + \
            """
psf-Cardinale-Sine-focus=0.0
psf-GL-ZPos=2000.0
psf-Koehler-dMid=3.0
psf-Lorentz-focus=0.0
psf-TV-TI=150.0
psf-Koehler-dTop=1.5
psf-Koehler-n1=1.0
psf-Circular-Pupil-axial=Linear
psf-Lorentz-defocus=100.0
psf-Koehler-n0=1.5
psf-Circular-Pupil-focus=0.0
psf-Koehler-dBot=6.0
psf-Defocus-ZI=2000.0
psf-VRIGL-NS2=1.4
psf-VRIGL-NS1=1.33
psf-Double-Helix-defocus=100.0
psf-TV-ZPos=2000.0
psf-Cosine-defocus=100.0
Scale=Linear
""" + \
            "psf-BW-NI=%f\n" % refr_ind_immersion + \
            "psf-GL-NS=%f\n" % refr_ind_immersion + \
            "Lambda=%f" % wavelength + \
            """
psf-GL-NI=1.5
psf-Astigmatism-defocus=100.0
ResAxial=250.0
psf-GL-TI=150.0
psf-Double-Helix-axial=Linear
""" + \
            "LUT=%s" % LUT + \
            """
psf-VRIGL-RIvary=Linear
""" + \
            "NZ=%d" % NZ + \
            """
psf-Double-Helix-focus=0.0
""" + \
            "NY=%d\n" % NY + \
            "NX=%d\n" % NX + \
            "psf-VRIGL-accuracy=%s" % accuracy + \
            """
psf-Gaussian-axial=Linear
psf-VRIGL-ZPos=2000.0
psf-Gaussian-focus=0.0
psf-Cardinale-Sine-defocus=100.0
psf-VRIGL-NI=1.5
Type=16-bits
""" + \
            "NA=%f" % NA + \
            """
psf-Oriented-Gaussian-defocus=100.0
psf-VRIGL-NG=1.5
psf-VRIGL-TI=150.0
psf-Defocus-DMid=1.0
psf-VRIGL-TG=170.0
psf-Defocus-K=275.0
psf-Cosine-axial=Linear
psf-TV-NS=1.0
psf-Cosine-focus=0.0
psf-Defocus-DTop=30.0
""" + \
            "psf-GL-accuracy=%s" % accuracy

    with open(fname, 'w') as f:
        f.write(output)
