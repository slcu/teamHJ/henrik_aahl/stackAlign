#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 16 13:06:55 2018

@author: henrik
"""

import pandas as pd
import numpy as np
import os

data = pd.read_csv('/home/henrik/stretching.txt', sep='\t', skiprows=11)
data = data.sort_values('    #file')
p = [eval(ii) for ii in data.pairs.values]
p = np.array([[jj[1] for jj in p[ii]] for ii in xrange(len(p))])
p = np.array([np.diff(ii) for ii in p])
p = np.array([ii[:-5] for ii in p]) # blanket removal


for ii in xrange(len(p)):
    first = 0
    for jj in xrange(len(p[ii])):
        if p[ii][jj] < 18 or p[ii][jj] > 26:
            first += 1
        else:
            break
    p[ii] = p[ii][first:]

for ii in xrange(len(p)):
    last = 0
    for jj in xrange(len(p[ii])):
        if p[ii][::-1][jj] < 18  or p[ii][::-1][jj] > 26:
            last += 1
        else:
            break
    p[ii] = p[ii][::-1][last:][::-1]

p = np.array(p)

mask = np.array([np.any(ii > 30) for ii in p])
for ii in xrange(len(p)):
    if mask[ii]:
        p[ii] = np.zeros(0, dtype= np.int64)


p = np.array([np.mean(ii) for ii in p])
p[np.isnan(p)] = 20
p[p < 20] = 20

p = p / 20.

new_zres = 0.25 / p


import phenotastic.file_processing as fp
def listdir(path, include=None, exclude=None, full=True):
    files = os.listdir(path)
    files = np.array(files)

    if full:
        files = np.array(map(lambda x: os.path.join(path, x), files))

    # Include
    if isinstance(include, str):
        files = np.array(filter(lambda x: include in x, files))
    elif isinstance(include, (list, np.ndarray)):
        matches = np.array([np.array([inc in ii for ii in files]) for inc in include])
        matches = np.any(matches, axis=0)
        files = files[matches]

    # Exclude
    if isinstance(exclude, str):
        files = np.array(filter(lambda x: exclude not in x, files))
    elif isinstance(exclude, (list, np.ndarray)):
        matches = np.array([np.array([exc in ii for ii in files]) for exc in exclude])
        matches = np.logical_not(np.any(matches, axis=0))
        files = files[matches]

    return files

indir = '/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h_STACKREG'
in_dir = listdir('/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h_STACKREG', exclude=['5um'])
in_dir = np.sort(in_dir)
files = data['    #file'].values
#files = map(lambda x: x + '.tif', files)
files = np.sort(files)

def mkdir(path):
    import os
    if not os.path.exists(path):
        os.makedirs(path)

outdir = indir + '_UNSTRETCHED'
mkdir(outdir)
for ii in xrange(len(in_dir)):
    fname = files[ii]
    print(fname)
    f = fp.tiffload(in_dir[ii])
    metadata = f.metadata

    if 'voxelsizez' in metadata:
        metadata['voxelsizez'] = new_zres[ii]
    metadata['spacing'] = new_zres[ii]

    fp.tiffsave(os.path.join(outdir, os.path.splitext(os.path.basename(fname))[0]),
                data=f.data, metadata=metadata, resolution=[metadata['spacing'],
                                                            metadata['voxelsizey'],
                                                            metadata['voxelsizex']])




