#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 21 14:56:58 2018

@author: henrik
"""
import os


def add_suffix(fname, suffix, output_dir=""):
    fnamebase = os.path.basename(fname)
    splitext = os.path.splitext(fnamebase)

    if output_dir == "":
        outfname = os.path.join(os.path.dirname(
            fname), splitext[0] + '_topcrop' + splitext[-1])
    else:
        outfname = os.path.join(
            output_dir, splitext[0] + '_topcrop' + splitext[-1])

    return outfname


def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)
