#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 21 17:30:39 2018

@author: henrik
"""

import time
import tifffile as tiff
import os
import numpy as np
from scipy.ndimage.filters import gaussian_filter
#from timagetk.components import SpatialImage
import functools
#import SimpleITK as sitk
from pystackreg import StackReg
import phenotastic.file_processing as fp
from skimage.transform import warp

#dir_ = "/home/henrikahl/tiffs/"
#dir_ = "/home/henrik/projects/stackAlign/stackAlign_output/cropped/"
#rapid_file = dir_ + 'pWUS-3X-VENUS-pCLV3-mCherry-on-NPA-1-0h-mCherry-0.7-Gain-800-5um_topcrop.tif'
#slow_file = dir_ + 'pWUS-3X-VENUS-pCLV3-mCherry-on-NPA-1-0h-mCherry-0.7-Gain-800_topcrop.tif'

#rapid_file = '/home/henrik/data/180129-pWUS-3X-VENUS-pCLV3-mCherry-Timelapse/on_NPA/pWUS-3X-VENUS-pCLV3-mCherry-on-NPA-1-0h-mCherry-0.7-Gain-800-5um.lsm'
#slow_file = '/home/henrik/data/180129-pWUS-3X-VENUS-pCLV3-mCherry-Timelapse/on_NPA/pWUS-3X-VENUS-pCLV3-mCherry-on-NPA-1-0h-mCherry-0.7-Gain-800.lsm'

#def registerSubtract(slices, method='rigid_registration'):
#    if not method == "":
#        sr = StackReg(StackReg.RIGID_BODY)
#        new = sr.register(slices
##        elastixImageFilter = sitk.ElastixImageFilter()
##        elastixImageFilter.SetFixedImage(
##            sitk.GetImageFromArray(slices[0][..., 0]))
##        elastixImageFilter.SetMovingImage(
##            sitk.GetImageFromArray(slices[1][..., 0]))
##        elastixImageFilter.SetParameterMap(
##            sitk.GetDefaultParameterMap("rigid"))
##        elastixImageFilter.SetNumberOfThreads(1)
##        elastixImageFilter.Execute()
#        new = np.array(sitk.GetArrayFromImage(
#            elastixImageFilter.GetResultImage()), dtype='float32')
#    else:
#        new = slices[1].get_array()[..., 0]
#    dist = np.sum(np.abs(new - slices[0].get_array()[..., 0]))
#    return dist
#def compare_images(ref, mov, method='rigid'):
#    if method == 'rigid':
#        sr = StackReg(StackReg.RIGID_BODY)
#    elif method == 'translation':
#        sr = StackReg(StackReg.TRANSLATION)
#    elif method == 'affine':
#        sr = StackReg(StackReg.AFFINE)
#    elif method == 'bilinear':
#        sr = StackReg(StackReg.BILINEAR)
#    elif method == 'scaled_rotation':
#        sr = StackReg(StackReg.SCALED_ROTATION)
#
#    if ref.ndim == 3:
#        ref_ = np.mean(ref, axis=0)
#        mov_ = np.mean(mov, axis=0)
#    else:
#        ref_ = ref
#        mov = mov_
#
#    fmat = sr.register(ref_, mov_)
#    fitted = warp(mov_, fmat, preserve_range=True)
#
#    dist = np.sum(np.abs(fitted - ref_))
#    return dist


def match_all(bigimage, smallimage, weight='relative_intensity', measure='euclid'):
    from phenotastic.misc import match_shape
    from skimage.feature import register_translation

    nbig = bigimage.shape[0]
    nsmall = smallimage.shape[0]

    bigimage = np.mean(bigimage, axis=1)
    smallimage = np.mean(smallimage, axis=1)
    
    shape = np.max([bigimage.shape[1:], smallimage.shape[1:]], axis=0)
    bigimage = match_shape(bigimage, tuple(np.insert(shape, 0, bigimage.shape[0])))
    smallimage = match_shape(smallimage, tuple(np.insert(shape, 0, smallimage.shape[0])))

    pairs = []
    sr = StackReg(StackReg.RIGID_BODY)

    # Find score for each slice
    for sindex in xrange(nsmall):
        print('slice: ', sindex)
        # Fit to given slice
        stack = np.insert(bigimage, 0, smallimage[sindex], axis=0)
        stack = sr.register_transform_stack(stack, reference='first')
        
        # Calculate distances
        if measure == 'euclid':
            distances = np.sum(np.abs(stack - stack[0]), axis=(1, 2))[1:]
        elif measure == 'corr' or measure == 'correlation':
            distances = -np.array([np.corrcoef(stack[ii].ravel(), stack[0].ravel())[0, 1] for ii in xrange(1, nbig)])
        else:
            print('Measure \'' + measure + '\' not understood. Using option \'euclid\'')
            distances = np.sum(np.abs(stack - stack[0]), axis=(1, 2))[1:]
            
        slice_scores = zip(distances, [sindex] * nbig, xrange(nbig))
        pairs.append(slice_scores)
    scores = np.array([np.array(ii) for ii in pairs])

    if weight == 'relative_intensity':
        print ('Not yet implemented')
        weights = []
        for img in smallimage:
            weights.append(np.mean(np.sort(img.ravel())[::-1][:int(0.33*len(img.ravel()))]))
        weights = np.array(weights)
            
#        weights = np.array([np.max(ii) / np.max(smallimage) for ii in smallimage])
#        min_val = np.min(np.array([scores[jj][:, 0] for jj in xrange(len(scores))]))
#        for ii in xrange(len(scores)):
#            scores[ii][:, 0] /= np.abs(np.min([scores[jj][:,0] for jj in xrange(len(scores))]))
            

    else:
        weights = np.array([1. for ii in smallimage])

    for ii in xrange(len(scores)):
        scores[ii][:, 0] *= -1
    mmax = np.max([scores[ii][:,0] for ii in xrange(len(scores))])
    mmin = np.min([scores[ii][:,0] for ii in xrange(len(scores))])

    for ii in xrange(len(scores)):
        scores[ii][:, 0] = (scores[ii][:, 0] - mmin) / (mmax-mmin)
        scores[ii][:, 0] *= (weights / np.max(weights))[ii]

    return scores

def traceback(scores, path, pairs, ii, jj, rightScore, downScore):
    matchScore = pairs[ii - 1][jj - 1, 0]
    wayHere = np.argwhere(
        scores[ii, jj] == np.array([
            scores[ii - 1, jj] + rightScore,
            scores[ii - 1, jj - 1] + matchScore,
            scores[ii,     jj - 1] + downScore
        ]))[0][0]
    if wayHere == 0:
        path.append((ii - 1, jj))
    elif wayHere == 1:
        path.append((ii - 1, jj - 1))
    else:
        path.append((ii, jj - 1))


def score_cell(scores, pairs, ii, jj, rightScore, downScore):
    scores[ii, jj] = np.max([
        scores[ii - 1, jj] + rightScore,
        scores[ii - 1, jj - 1] + pairs[ii - 1][jj - 1, 0],
        scores[ii, jj - 1] + downScore
    ])


def align(pairs, miss_small_edge, miss_small_mid, miss_big_edge, miss_big_mid,
          outfile=''):

    nsmall, nbig = np.array(pairs[-1][-1, 1:3] + 1, dtype='int')
    scores = np.zeros((nsmall + 1, nbig + 1))

    ''' Create scoring matrix '''
    # Set top-left-mid-right-bottom-corner
    for ii in xrange(1, nsmall + 1):
        scores[ii, 0] = scores[ii - 1, 0] + miss_small_edge  # go right
    for jj in xrange(1, nbig + 1):
        scores[0, jj] = scores[0, jj - 1] + miss_big_edge  # go down
    for ii in xrange(1, nsmall):
        for jj in xrange(1, nbig):
            score_cell(scores, pairs, ii, jj, miss_small_mid, miss_big_mid)
    for ii in xrange(1, nsmall):
        score_cell(scores, pairs, ii, nbig, miss_small_edge, miss_big_mid)
    for jj in xrange(1, nbig):
        score_cell(scores, pairs, nsmall, jj, miss_small_mid, miss_big_edge)
    score_cell(scores, pairs, nsmall,
               nbig, miss_small_edge, miss_big_edge)

    ''' Rebuild '''
    path = []
    mVal = np.max(scores)
    found = False
    for ii in xrange(nsmall + 1):
        for jj in xrange(nbig + 1):
            if scores[ii, jj] == mVal and (ii == nsmall or jj == nbig):
                path.append((ii, jj))
                found = True
                break
        if found:
            break

    # If on corner
    if path[-1] == (nsmall, nbig):
        ii, jj = path[-1]
        traceback(scores, path, pairs, ii, jj, miss_small_edge, miss_big_edge)

        if path[-1][0] == nsmall:
            while path[-1][0] == nsmall and path[-1][0] > 0:
                ii, jj = path[-1]
                traceback(scores, path, pairs, ii, jj,
                          miss_small_mid, miss_big_edge)
        elif path[-1][1] == nbig:
            while path[-1][1] == nbig and path[-1][1] > 0:
                ii, jj = path[-1]
                traceback(scores, path, pairs, ii, jj,
                          miss_small_edge, miss_big_mid)
    # If on right side, and not corner
    elif path[-1][0] == nsmall:
        while path[-1][0] == nsmall and path[-1][0] > 0:
            ii, jj = path[-1]
            traceback(scores, path, pairs, ii, jj, miss_small_mid, miss_big_edge)
    # If on bottom, and not corner
    elif path[-1][1] == nbig:
        while path[-1][1] == nbig and path[-1][1] > 0:
            ii, jj = path[-1]
            traceback(scores, path, pairs, ii, jj, miss_small_edge, miss_big_mid)

    # Build rest of path
    while path[-1][0] > 0 and path[-1][1] > 0:
        ii, jj = path[-1]
        traceback(scores, path, pairs, ii, jj, miss_small_mid, miss_big_mid)

    ''' Add potential head and tail '''
    # head
    add = []
    if path[-1][0] > path[-1][1]:
        add = zip(range(path[-1][0]), [0] * path[-1][0])[::-1]
    elif path[-1][1] > path[-1][0]:
        add = zip([0] * path[-1][1], range(path[-1][1]))[::-1]
    path = path + add

    # tail
    add = []
    if path[0][0] != nsmall:
        add = zip(range(nsmall, path[-1][0], -1),
                  [nbig] * (nsmall - path[-1][0]))
    elif path[0][1] != nbig:
        add = zip([nsmall] * (nbig - path[-1][1]),
                  range(nbig, path[-1][1], -1))
    path = add + path

    ''' Piece together string (could do this before, but whatever)'''
    str1 = ""
    str2 = ""
    p = []
    path = path[::-1]
    for ii in xrange(0, len(path) - 1):
        if path[ii + 1][0] == path[ii][0]:
            str1 += '(' + '-' * len(str(path[ii][1])) + ')'
            str2 += '(' + str(path[ii][1]) + ')'
        elif path[ii + 1][1] == path[ii][1]:
            str1 += '(' + str(path[ii][0]) + ')'
            str2 += '(' + '-' * len(str(path[ii][0])) + ')'
        else:
            dist = np.abs(len(str(path[ii][0])) - len(str(path[ii][1])))
            if len(str(path[ii][0])) >= len(str(path[ii][1])):
                str1 += '(' + str(path[ii][0]) + ')'
                str2 += '(' + str(path[ii][1]) + '-' * dist + ')'
            else:
                str1 += '(' + str(path[ii][0]) + '-' * dist + ')'
                str2 += '(' + str(path[ii][1]) + ')'
            p.append((path[ii]))

#    if printAlignment and outfile != "":
#        with open(os.path.splitext(outfile)[0] + '-alignment.dat', 'a') as f:
#            f.writelines([str1, "\n", str2, "\n\n"])

    p = np.array([pairs[ii[0]][ii[1], :] for ii in p])
    p = p[:, np.array([1, 2, 0])]
    return p

# TODO: allow for changing alignment file name
def z_correct_dir(dir_, outfile, rapidId='-5um', refId='reference',
                  method='rigid_registration', gaussian_filter_val=0,
                  pruneBottom=0, pruneTop=0, printAlignment=False, pool=None):
    files = os.listdir(dir_)
    files = [x for x in files if x.endswith('.tif') or x.endswith('.tiff')]
    refFiles = [x for x in files if refId in x]
    files = np.setdiff1d(files, refFiles)
    slow_files = [x for x in files if rapidId in x]
    rapid_files = np.setdiff1d(files, slow_files)

    if len(slow_files) != len(rapid_files):
        raise ValueError("Number of files of each type not equally matched.")

    slow_files = np.sort(slow_files)
    rapid_files = np.sort(rapid_files)
    slow_files = map(lambda x: os.path.join(dir_, x), slow_files)
    rapid_files = map(lambda x: os.path.join(dir_, x), rapid_files)

    file_pairs = zip(slow_files, rapid_files)
#    print(file_pairs)

    with open(outfile, 'w') as f:
        s = '''# File containing output data for stretching factor correction. Below
        # follow the input arguments used for this specific output:
        #
        # dir={}
        # outfile={}
        # rapidId={}
        # refId={}
        # method={}
        # gaussian_filter_val={}
        # pruneBottom={}
        # pruneTop={}
        '''.format(dir_, outfile, rapidId, refId, method, gaussian_filter_val, pruneBottom, pruneTop)
        f.writelines(s)

        f.writelines('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(
                "#file", 'meanStretch', 'medianStretch', 'modeStretch', 'stdStretch',
                "time", "pairs", "scores"))

        if printAlignment:
            with open(os.path.splitext(outfile)[0] + '-alignment.dat', 'w') as fa:
                fa.writelines(s)

#    for fp in xrange(len(file_pairs)):

    for pair in file_pairs:
        print(pair)

    if not pool is None:
        pool.map(functools.partial(align_pair, outfile=outfile, method=method,
                                gaussian_filter_val=gaussian_filter_val,
                                pruneBottom=pruneBottom, pruneTop=pruneTop), file_pairs)
    else:
        for pair in file_pairs:
            align_pair(pair, outfile=outfile, method=method,
                       gaussian_filter_val=gaussian_filter_val,
                       pruneBottom=pruneBottom, pruneTop=pruneTop)
#fp = file_pairs[0]


def align_pair(pair, outfile,
               method='rigid', gaussian_filter_val=0,
               pruneBottom=0, pruneTop=0, printAlignment=False):
    timeS = time.time()
    rpath = pair[0]
    spath = pair[1]

    rfile = fp.tiffload(rpath)
    sfile = fp.tiffload(spath)

    pairs = match_all(rfile, sfile, method=method,
                      gaussian_filter_val=gaussian_filter_val)

    nRapidSlices, nSlowSlices = map(int, pairs[-1][-1, 1:3])
    rSpace = rfile.metadata['spacing']
    sSpace = sfile.metadata['spacing']
    falseSliceRatio = rSpace / sSpace

    # TODO: Change this and include information in output file
    missSlowMid = 1. / falseSliceRatio
    missSlowEdge = missSlowMid
    missRapidMid = np.finfo(np.float).min
    missRapidEdge = 1. / falseSliceRatio

    matches = align(pairs, missSlowMid=missSlowMid,
                    missSlowEdge=missSlowEdge, missRapidMid=missRapidMid,
                    missRapidEdge=missRapidEdge, outfile=outfile,
                    printAlignment=printAlignment)

    # TODO: Filter edges on quality?
    sfinalPairs = [(int(pa[0]), int(pa[1])) for pa in matches]
    sscores = [int(pa[2]) for pa in matches]

#    matches = matches[pruneBottom:(len(matches) - pruneTop)]

    finalPairs = [(int(pa[0]), int(pa[1])) for pa in matches]
#      scores     = [int(p[2]) for p in matches]

    def mode(x):
        if len(x) > 0:
            return max(list(x), key=list(x).count)
        else:
            return np.nan

    dists = np.diff([ii[1] for ii in finalPairs]) / (rSpace / sSpace)
    dists = np.sort(dists)

    meanStretchFactor = np.mean(dists)
    medianStretchFactor = np.median(dists)
    modeStretchFactor = mode(dists[::-1])
    stdStretchFactor = np.std(dists)

    time_end = time.time() - timeS

    with open(outfile, 'a') as f:
        fname = os.path.splitext(os.path.basename(spath))[0]
        f.write('%s\t%f\t%f\t%f\t%f\t%f\t%s\t%s\n' %
                (fname, meanStretchFactor, medianStretchFactor,
                 modeStretchFactor, stdStretchFactor, time_end, sfinalPairs, sscores))



