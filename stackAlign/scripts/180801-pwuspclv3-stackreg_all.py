#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 00:00:17 2018

@author: henrik
"""
import phenotastic.file_processing as fp
from pystackreg import StackReg
from skimage.transform import warp
from phenotastic.misc import listdir
import numpy as np
import gc

dir_ = '/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h/'
files = listdir(dir_)
files = map(lambda x: dir_ + x, files)
files.sort()

outdir_ = '/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h_STACKREG/'

for p1 in files:
    f1 = fp.tiffload(p1)
    metadata = f1.metadata

    not_empty = np.sum(f1.data, axis=(1, 2, 3)) > 0
    f1.data = f1.data[not_empty]

    new = f1.data
    new_mean = np.mean(new, axis=1)

    sr = StackReg(StackReg.RIGID_BODY)
    tmats = sr.register_stack(new_mean, reference='previous')

    for ii in xrange(new.shape[0]):
        for jj in xrange(new.shape[1]):
            new[ii, jj] = warp(new[ii, jj], tmats[ii], preserve_range=True)

    new_name = outdir_ + os.path.basename(p1)[:-4] + '.tif'
    fp.tiffsave(new_name, new,
                resolution=[metadata['voxelsizez'],
                            metadata['voxelsizey'],
                            metadata['voxelsizex']],
                            metadata=metadata)
    gc.collect()

