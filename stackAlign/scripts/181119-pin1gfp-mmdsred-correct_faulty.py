#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 14:06:40 2018

@author: henrik
"""

import phenotastic.file_processing as fp
from pystackreg import StackReg
from skimage.transform import warp
from phenotastic.misc import listdir, mkdir, autocrop
import numpy as np
import gc
import tifffile as tiff
import os
from stackAlign.deconv import create_psf, deconvolve
import mahotas as mh
from skimage.exposure import rescale_intensity
from skimage.transform import EuclideanTransform, warp
from scipy.ndimage.measurements import center_of_mass


def run_reg(data):
    channelmax = np.max(data, axis=1)
    channelmax[channelmax < mh.otsu(channelmax.astype('uint16'), ignore_zeros=True)] = 0
    channelmax = rescale_intensity(channelmax, out_range='uint8').astype('uint8')
    
    # Perform stack regularisation
    sr = StackReg(StackReg.RIGID_BODY)
    tmats = sr.register_stack(channelmax, reference='previous')
    for ii in xrange(data.shape[0]):
        for jj in xrange(data.shape[1]):
            data[ii, jj] = warp(data[ii, jj], tmats[ii], preserve_range=True)
    return data


fname = '/media/henrik/Seagate Expansion Drive/181113-PIN-GFP-MM-dsRed-Timelapse-24h/181113-PIN-GFP-MM-dsRed-on_npa-5-72h.lsm'
f = fp.tiffload(fname)
sr = StackReg(StackReg.RIGID_BODY)
data1 = f.data[:187].copy()
data1 = run_reg(data1)

data2 = f.data[187:].copy()
data2 = run_reg(data2)

# Get reference point and combine
c1 = center_of_mass(np.max(data1[-1], axis=0) > mh.otsu(np.max(data1[-1], axis=0)))
c2 = center_of_mass(np.max(data2[0], axis=0) > mh.otsu(np.max(data2[0], axis=0)))
for ii in xrange(data2.shape[0]):
    for jj in xrange(data2.shape[1]):
        data2[ii, jj] = warp(data2[ii, jj], EuclideanTransform(translation=(c2[1] - c1[1], 
             c2[0] - c1[0])), preserve_range=True)
data = np.vstack([data1, data2])
data = run_reg(data)

fp.tiffsave(fname[:-4] + '_corrected.tif', data, metadata=f.metadata, resolution=fp.get_resolution(f))


