#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 21 14:37:33 2018

@author: henrik
"""
import functools
import argparse
import os
import multiprocessing
from stackAlign.misc import mkdir
import numpy as np
import stackAlign.imageProc as ip
import subprocess
import stackAlign.background_extraction as bg
import tifffile as tiff
import copy
from stackAlign.alignment import z_correction
import pandas as pd
import phenotastic.file_processing as fp

if_default  = '/home/henrikahl/home3/microscopy/teamhj/Henrik-Aahl/180312-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-6h/off_NPA'
of_default  = os.getcwd() + 'stackAlign_output'
saf_default = '/home/henrikahl/projects/stackAlign'
rid_default = '-5um'
refid_default = 'reference'
r_default   = [515, 545, 580, 633, 654, 735]
di_default  = [10, 8, 3]
#exit()
parser = argparse.ArgumentParser(description='')
parser.add_argument('input_folder',        type=str, default=if_default)
parser.add_argument('output_folder',       type=str, default=of_default)
parser.add_argument('--stackalign_folder', type=str, default=saf_default)
parser.add_argument('--rapid_id',          type=str, default=rid_default)
parser.add_argument('--reference_id',      type=str, default=refid_default)
parser.add_argument('--nproc',             type=int, default=1)
parser.add_argument('--range',             default=r_default, action='append',
                    nargs=2)
parser.add_argument('--deconv_iters',      default=di_default, nargs='+')

args = parser.parse_args()


#
################################################################################
#
#
INPUT_PATH      = args.input_folder
OUTPUT_PATH     = args.output_folder
STACKALIGN_PATH = args.stackalign_folder
RAPID_ID        = args.rapid_id
NPROCESSORS     = args.nproc
DECONV_ITERS    = args.deconv_iters
REFERENCE_ID    = args.reference_id

#INPUT_PATH      ="/home/henrik/data/180129-pWUS-3X-VENUS-pCLV3-mCherry-Timelapse/on_NPA"
#INPUT_PATH      ="/home/henrikahl/home3/microscopy/teamhj/Henrik-Aahl/180312-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-6h/on_NPA"
#INPUT_PATH      ="/home/henrikahl/test_data"
#INPUT_PATH      = os.path.join(os.path.expanduser('~'), 'projects/stackAlign/data')
#OUTPUT_PATH     = INPUT_PATH
STACKALIGN_PATH = os.path.abspath('')

#OUTPUT_PATH     = os.path.join(os.getcwd(), 'stackAlign_output')
#OUTPUT_PATH     = '/home/henrikahl/final_test'

#STACKALIGN_PATH = '/home/henrikahl/projects/stackAlign'
#STACKALIGN_PATH = '/home/henrik/projects/stackAlign'

RAPID_ID = '-5um'
REFERENCE_ID = 'reference'

ALIGNMENT_PRUNE_BOTTOM = 2
ALIGNMENT_PRUNE_TOP = 4
ALIGNMENT_GAUSSIAN_FILTER = 0
ALIGNMENT_METHOD = 'rigid'

#BG_EXT_THRES = 2
#BG_EXT_ITER = 10
#NPROCESSORS = 3
#r_default   = [515, 545, 580, 633, 654, 735]
#DECONV_ITERS    = [10, 8, 3]
#WAVELENGTHS = [1,2,3]
NUM_APERTURE = 1.0
REFR_INDEX = 1.333
WAVELENGTHS = [514, 561, 561]
CROP_THRES = 0
#RANGES      = np.reshape(args.range, (len(args.range) / 2, 2))
#WAVELENGTHS = np.mean(RANGES, axis=1)

''' DIRECTORIES AND FILES '''
STRETCH_FILE    = os.path.join(OUTPUT_PATH, 'stretching.txt')
ALIGNMENT_FILE  = os.path.join(OUTPUT_PATH, 'alignment.txt')
TIFFS_PATH      = os.path.join(OUTPUT_PATH, 'tiffs')
CROP_PATH       = os.path.join(OUTPUT_PATH, 'cropped')
DECONV_PATH     = os.path.join(OUTPUT_PATH, 'deconv')
FILTERED_PATH   = os.path.join(OUTPUT_PATH, 'filtered')
MIPS_PATH       = os.path.join(OUTPUT_PATH, 'mips')
PSF_PATH        = os.path.join(OUTPUT_PATH, 'PSFs')
PSF_CONFIG_PATH = os.path.join(PSF_PATH,    'configs')

''' SOFTWARE '''
################################################################################
##############################  RUN STUFF  #####################################
################################################################################
''' Create output directories '''
print('Creating output directories...')
mkdir(OUTPUT_PATH)
mkdir(DECONV_PATH)

#for ii in xrange((len(WAVELENGTHS))):
#  mkdir(os.path.join(OUTPUT_PATH, 'channel%d' % (ii + 1)))

################################################################################
'''' Converting to tiffs '''
p = multiprocessing.Pool(NPROCESSORS)

################################################################################
''' Correct for stretching '''
z_correction(CROP_PATH, STRETCH_FILE, rapidId=RAPID_ID, refId=REFERENCE_ID,
             method=ALIGNMENT_METHOD,
             gaussian_filter_val=ALIGNMENT_GAUSSIAN_FILTER,
             pruneBottom=ALIGNMENT_PRUNE_BOTTOM,
             pruneTop=ALIGNMENT_PRUNE_TOP,
             printAlignment=True,
             p=p)

def correct_stretch(fname, stretch_file=STRETCH_FILE, var='meanStretch',
                    allow_compression=False):
    fnamebase = os.path.splitext(os.path.basename(fname))[0]

    # Read stretch file
    data = pd.read_table(STRETCH_FILE, skiprows=11, sep = "\t",
                         names=['file', 'meanStretch', 'medianStretch', 'modeStretch',
                                'stdStretch', 'time', 'pairs', 'scores'])

    try:
      stretch = data[(data['file'] == fnamebase).values][var].values[0]
    except (KeyError, IndexError):
        print('''Error: File {fnamebase} could not be found. Setting stretch to
              1.0.'''.format(fnamebase=fnamebase))
        stretch = 1.0

    if not allow_compression and stretch < 1.0:
      stretch = 1.0

    # Correct metadata
    file_ = fp.tiffload(fname)
    meta = file_.metadata
    meta['spacing'] /= stretch
    #voxelsize[2] /= stretch
    if 'voxelsizez' in meta:
        meta['voxelsizez'] /= stretch

    resolution = [meta['spacing'], 1, 1]

    if 'voxelsizey' in meta:
        resolution[1] = meta['voxelsizey']
    if 'voxelsizex' in meta:
        resolution[2] = meta['voxelsizex']

    fp.tiffsave(fname, file_.data, resolution=resolution, metadata=meta)

print('Correcting for stretching...')

def listdir(path, include=None, exclude=None, full=True):
    files = os.listdir(path)

    if full:
        files = map(lambda x: os.path.join(path, x), files)

    # TODO
    if include is not None:
        files = filter(lambda x: np.any(np.in1d(include, x)), files)
    if exclude is not None:
        files = filter(lambda x: not np.all(np.in1d(exclude, x)), files)

    return files

cfiles = listdir(INPUT_PATH)

sfiles = filter(lambda x: RAPID_ID not in x, cfiles)
p.map(functools.partial(correct_stretch, stretch_file=STRETCH_FILE,
                        var='meanStretch'), sfiles)


def remove_background(fname, factor=1.5, slice_=0):
    file_ = fp.tiffload(fname)
    data = file_.data
    nchannels = data.shape[1]
    metadata=file_.metadata

    resolution = [metadata['voxelsizez'],
                  metadata['voxelsizey'],
                  metadata['voxelsizex']]

    for ii in xrange(nchannels):
        threshold = factor * np.median(data[slice_, ii])
        cdata = data[:, ii]
        cdata[cdata < threshold] = 0
        data[:, ii] = cdata

    fp.tiffsave(fname, data, resolution=resolution, metadata=metadata)


p.map(functools.partial(remove_background), sfiles)

################################################################################

#for ii in xrange(fp.tiffload(files[0]).data.shape[1]):
#    mkdir(os.path.join(OUTPUT_PATH, 'C' + str(ii+1)))

#print('Splitting channels...')
#def split_channels(fname, output_dir=''):
#    file_ = fp.tiffload(fname)
#    data = file_.data
#    nchannels = data.shape[1]
#
#    for ii in xrange(nchannels):
#        cdata = data[:, [ii]]
#        fp.tiffsave(os.path.join(output_dir, 'C' + str(ii + 1) + '-' + fname),
#                    data=cdata, resolution=[file_.metadata['voxelsizez'],
#                                            file_.metadata['voxelsizey'],
#                                            file_.metadata['voxelsizex']])




#################################################################################
#''' Perform deconvolution '''
## (fname, output_dir, iterations, method='LR', zshape,
##                            rshape, zdims, rdims, ex_wavelen, em_wavelen,
##                            num_aperture, refr_index, pinhole_radius,
##                            pinhole_shape='square',
##                            psf_type=(psf.ISOTROPIC | psf.CONFOCAL),
##                            substep=1, clip=False):
#
#ch1 = os.listdir(OUTPUT_PATH + '/channel1')
#ch2 = os.listdir(OUTPUT_PATH + '/channel2')
#ch3 = os.listdir(OUTPUT_PATH + '/channel3')
#ch1 = map(lambda x: os.path.join(OUTPUT_PATH, "channel1", x), ch1)
#ch2 = map(lambda x: os.path.join(OUTPUT_PATH, "channel2", x), ch2)
#ch3 = map(lambda x: os.path.join(OUTPUT_PATH, "channel3", x), ch3)
#
#all_files_channels = ch1 + ch2 + ch3
#all_files_channels.sort()
#cfiles = copy.deepcopy(all_files_channels)
#cfiles = filter(lambda x: RAPID_ID not in x, cfiles)
#
##def deconvolve_file_to_file(fname, output_dir, iterations, method, num_aperture=None,
##                            refr_index=None, zshape=None, rshape=None, zdims=None,
##                            rdims=None, ex_wavelen=None, em_wavelen=None,
##                            pinhole_radius=None, pinhole_shape='square',
##                            psf_type=(psf.ISOTROPIC | psf.CONFOCAL),
##                            substep=1, clip=False, magnification=20):
#
#p.close(); p.join(); del p
#
#p = multiprocessing.Pool(NPROCESSORS)
#import deconv
#c1 = filter(lambda x: 'C1' in x, all_files_channels)
#c2 = filter(lambda x: 'C2' in x, all_files_channels)
#c3 = filter(lambda x: 'C3' in x, all_files_channels)
#
#p.map(functools.partial(deconv.deconvolve_file_to_file, output_dir = DECONV_PATH, iterations=10, method='LR'), c1)
#p.map(functools.partial(deconv.deconvolve_file_to_file, output_dir = DECONV_PATH, iterations=8, method='LR'), c2)
#p.map(functools.partial(deconv.deconvolve_file_to_file, output_dir = DECONV_PATH, iterations=5, method='LR'), c3)
#
##p.map(deconvolve, all_files_channels)
#
#print('All done.')
#
#
##from analyse import intensity_projection_series_all
##ffiles = os.listdir(FILTERED_PATH)
##ffiles = map(lambda x: os.path.join(FILTERED_PATH, x), ffiles)
