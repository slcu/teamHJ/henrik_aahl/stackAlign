#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 20 13:34:29 2018

@author: henrikahl
"""

import os
import numpy as np
import tifffile as tiff
from timagetk.components import SpatialImage, imread, imsave
from timagetk.plugins import registration

from scipy.ndimage.morphology import binary_dilation, binary_erosion
from itertools import cycle


def si_correct_dim(sp_img):
  sp_img = np.reshape(sp_img, sp_img.shape + (1,) * (3 - len(sp_img.shape)))
  return SpatialImage(sp_img)

fname = "/home/henrik/data/180129-pWUS-3X-VENUS-pCLV3-mCherry-Timelapse/on_NPA/pWUS-3X-VENUS-pCLV3-mCherry-on-NPA-1-0h-mCherry-0.7-Gain-800.lsm"
fname_tiff = "/home/henrik/180312-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-6h/off_NPA/C1-pWUS-3XVENUS-pCLV3-mCherry-off_NPA-1-12h-2.0-750-0.5-770.tif"

def lsm_to_spatialimage(fname):
  sfile_ = tiff.TiffFile(fname)
  data   = np.array(sfile_.asarray(), dtype='uint16')
  meta   = sfile_.lsm_metadata
  meta["dim"]       = 3
  meta["shape"]     = (meta["DimensionX"], meta["DimensionY"], meta["DimensionZ"])
  meta["max"]       = np.max(data)
  meta["mean"]      = np.mean(data)
  meta["min"]       = np.min(data)
  meta["origin"]    = [meta["OriginX"]*1e6, meta["OriginY"]*1e6, meta["OriginZ"]*1e6]
  meta["type"]      = 'uint16'
  meta["extent"]    = [0,0,0]
  meta["voxelsize"] = (meta['VoxelSizeX']*1e6, meta['VoxelSizeY']*1e6, meta['VoxelSizeZ']*1e6)

  return sfile_

def read_SpatialImage(fname, dtype=np.uint16):
  sfile_ = imread(fname)
  meta   = sfile_.get_metadata()
  sfile_ = si_correct_dim(np.array(sfile_.get_array(), dtype=dtype))
  sfile_.get_metadata()['voxelsize'] = meta['voxelsize']

  return sfile_

def project_intensity(sp_img, dim, fct=np.max):
  return fct(sp_img, axis=dim)

def intensity_projection_stack_single(infiles, dim,
                                      method='rigid_registration', fct=np.max,
                                      LUT='Fire', show=False, outfile=""):
  images = map(read_SpatialImage, infiles)
  mimg   = [project_intensity(ii, dim=dim, fct=fct) for ii in images]
  mimg   = map(SpatialImage, mimg)
  mimg   = map(si_correct_dim,  mimg)

  for ii in xrange(1, len(mimg)):
    _, mimg[ii] = registration(mimg[ii], mimg[ii - 1],
                               method=method)

  mimg = np.concatenate(mimg, axis=2)

  if show:
    tiff.imshow(np.transpose(mimg, (2,0,1)))
  if outfile != "":
    imsave(outfile, mimg)

  return mimg

def intensity_projection_stack_all(infiles, method='rigid_registration',
                                   fct=np.max, LUT='Fire', show=False,
                                   outfile="", order='h'):
  c1 = intensity_projection_stack_single(infiles, 2, method=method, fct=fct,
                                         LUT=LUT, show=False, outfile="")
  c2 = intensity_projection_stack_single(infiles, 0, method=method, fct=fct,
                                         LUT=LUT, show=False, outfile="")
  c3 = intensity_projection_stack_single(infiles, 1, method=method, fct=fct,
                                         LUT=LUT, show=False, outfile="")

  import matplotlib.pyplot as plt
  fig = plt.figure()
  if order == "v":
    tiff.imshow(np.transpose(c1, (2,0,1)), subplot=311, figure=fig)
    tiff.imshow(np.transpose(c2, (2,0,1)), subplot=312, figure=fig)
    tiff.imshow(np.transpose(c3, (2,0,1)), subplot=313, figure=fig)
  else:
    tiff.imshow(np.transpose(c1, (2,0,1)), subplot=131, figure=fig)
    tiff.imshow(np.transpose(c2, (2,0,1)), subplot=132, figure=fig)
    tiff.imshow(np.transpose(c3, (2,0,1)), subplot=133, figure=fig)

def intensity_projection_series_single(infiles, dim, method='rigid_registration',
                                   fct=np.max, LUT='Fire', show=False,
                                   outfile=""):

  images = map(read_SpatialImage, infiles)
  mimg   = [project_intensity(ii, dim=dim, fct=fct) for ii in images]
  mimg   = map(SpatialImage, mimg)
  mimg   = map(si_correct_dim,  mimg)

  for ii in xrange(1, len(mimg)):
    _, mimg[ii] = registration(mimg[ii], mimg[ii - 1],
                               method=method)

  dx = (mimg[0].shape[0])
  output = np.zeros((len(mimg) * dx, mimg[0].shape[1], 1))
  for ii in xrange(len(mimg)):
    output[(ii * dx):((ii + 1) * dx)] = mimg[ii]

  if show:
    tiff.imshow(np.transpose(output, (1,0,2)))
  if outfile != "":
    imsave(outfile, output)

def intensity_projection_series_all(infiles, dim, method='rigid_registration',
                                   fct=np.max, LUT='Fire', show=False,
                                   outfile=""):
  c1 = intensity_projection_series_single(infiles, 2, method=method, fct=fct,
                                         LUT=LUT, show=False, outfile="")
  c2 = intensity_projection_series_single(infiles, 0, method=method, fct=fct,
                                         LUT=LUT, show=False, outfile="")
  c3 = intensity_projection_series_single(infiles, 1, method=method, fct=fct,
                                         LUT=LUT, show=False, outfile="")
  c1, c2, c3 = np.transpose(c1), np.transpose(c2), np.transpose(c3)

  dx = (c1[0].shape[0])
  dy = (c1[0].shape[1], c2[0].shape[1], c3[0].shape[1])
  output = np.zeros((len(c1) * dx, np.sum(dy), 1))

  for ii in xrange(len(c1)):
    output[(ii * dx):((ii + 1) * dx), (dy[0]):(dy[1])] = c1[ii]
    output[(ii * dx):((ii + 1) * dx), (dy[1]):(dy[1] + dy[2])] = c2[ii]
    output[(ii * dx):((ii + 1) * dx), (dy[1] + dy[2]):(dy[0] + dy[1] + dy[2])] = c3[ii]


  if outfile != "":
    imsave(outfile, output)

################################################################################
################################################################################
################################################################################

#inpath = "/home/henrikahl/output-off_NPA/mips"
#inpath = "/home/henrikahl/deconv3"
#inpath = "/home/henrikahl/conv-1"
inpath = "/home/henrik/180312-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-6h/off_NPA"
files = os.listdir(inpath)
files = map(lambda x: os.path.join(inpath, x), files)
files = filter(lambda x: not '-5um' in x, files)
files.sort()
c1 = filter(lambda x: 'C1' in x, files)[0]
plant1 = filter(lambda x: 'off_NPA-1' in x, files)
plant1 = filter(lambda x: '.tif' in x, plant1)

data = read_SpatialImage(c1)
#import morphsnakes as ms

def contour_fit_threshold(data, threshold=.8, smooth_iterate=3):
    contour = np.array(data < threshold * 0.5 * np.mean(data.flatten()), dtype='bool')
    contour = smooth_contour(contour, smooth_iterate) # TODO
    return contour

def step(data, contour, weighting):
    inside  = contour > 0
    outside = contour <= 0

    c0 = np.sum(data[outside]) / float(np.sum(outside))
    c1 = np.sum(data[inside]) / float(np.sum(inside))

    dres     = np.array(np.gradient(contour))
    abs_dres = np.abs(dres).sum(0)

    aux      = abs_dres * (weighting * (data - c1)**2 - 1. / float(weighting) * (data - c0)**2)
    contour[aux < 0] = 1
    contour[aux > 0] = 0
    return contour

def smooth_contour(contour, iterate=1):
    curvop = fcycle([SIoIS, ISoSI])
    for ii in xrange(iterate):
        contour = curvop(contour)
    return contour

def contour_fit(data, contour, weighting, iterate_smooth):
    iterations = np.min(np.shape(data))
    contour    = set_contour_to_box()
    for ii in range(iterations):
        contour = step(weighting)
        contour = smooth_contour(iterate_smooth)
        print("Contour fit: iteration %s / %s..." % (ii + 1, iterations))
    return contour

def set_contour_to_box(data):
    contour = getplanes(np.shape(data))
    contour = setedges(contour, 0)
    contour[0] = 0
    return contour

def setedges(array, value=0):
  array[[0, 0, -1, -1], [0, -1, 0, -1], :] = value
  array[:, [0, 0, -1, -1], [0, -1, 0, -1]] = value
  array[[0, -1, 0, -1], :, [0, 0, -1, -1]] = value
  return array

def getedges(shape):
    array = np.ones(shape, dtype='bool')
    array = setedges(array)
    return array

def setplanes(array, value=0):
    array[[0, -1], :, :] = value
    array[:, [0, -1], :] = value
    array[:, :, [0, -1]] = value
    return array

def getplanes(shape, invert=True):
    if invert:
        array = np.zeros(shape, dtype='bool')
        array = setplanes(array, value=1)
    else:
        array = np.ones(shape, dtype='bool')
        array = setplanes(array, value=0)
    return array

class fcycle(object):
    def __init__(self, iterable):
        self.funcs = cycle(iterable)

    def __call__(self, *args, **kwargs):
        f = next(self.funcs)
        return f(*args, **kwargs)

def SI(u, iterate=1):
    P = np.array([np.zeros((3, 3, 3)) for i in xrange(9)])
    P[0][:, :, 1] = 1
    P[1][:, 1, :] = 1
    P[2][1, :, :] = 1
    P[3][:, [0, 1, 2], [0, 1, 2]] = 1
    P[4][:, [0, 1, 2], [2, 1, 0]] = 1
    P[5][[0, 1, 2], :, [0, 1, 2]] = 1
    P[6][[0, 1, 2], :, [2, 1, 0]] = 1
    P[7][[0, 1, 2], [0, 1, 2], :] = 1
    P[8][[0, 1, 2], [2, 1, 0], :] = 1
    _aux = np.zeros((0), dtype='bool')
    if u.shape != _aux.shape[1:]:
        _aux = np.zeros((len(P),) + u.shape, dtype='bool')
    for ii in xrange(len(P)):
        _aux[ii] = binary_erosion(u, P[ii], iterations=iterate,
                                 mask=getedges(np.shape(u)))
    return np.max(_aux, axis=0)



def IS(u, iterate=1):
    P = np.array([np.zeros((3, 3, 3), dtype='bool') for ii in xrange(9)])
    P[0][:, :, 1] = 1
    P[1][:, 1, :] = 1
    P[2][1, :, :] = 1
    P[3][:, [0, 1, 2], [0, 1, 2]] = 1
    P[4][:, [0, 1, 2], [2, 1, 0]] = 1
    P[5][[0, 1, 2], :, [0, 1, 2]] = 1
    P[6][[0, 1, 2], :, [2, 1, 0]] = 1
    P[7][[0, 1, 2], [0, 1, 2], :] = 1
    P[8][[0, 1, 2], [2, 1, 0], :] = 1
    _aux = np.zeros((0), dtype='bool')
    if u.shape != _aux.shape[1:]:
        _aux = np.zeros((len(P),) + u.shape, dtype='bool')
    for ii in xrange(len(P)):
        _aux[ii] = binary_dilation(u, P[ii], iterations=iterate,
                                  mask=getedges(np.shape(u)))
    return np.min(_aux, axis=0)

def SIoIS(u):
    return SI(IS(u))

def ISoSI(u):
    return IS(SI(u))

#contour = contour_fit_threshold(data, threshold=.9, smooth_iterate=20)

#tiff.imshow(np.transpose(contour, (2,1,0)))
#tiff.imsave('testfile.tif', np.array(np.transpose(contour, (2,1,0)), np.uint16))




#
#
#A = AutoPhenotype(data = np.array(data, dtype='int8'))  # initialise object
#A.contour_fit_threshold(threshold=.8, smooth_iterate=10)
#
#tiff.imshow(np.transpose(A.contour, (2,0,1)))
#tiff.imshow(np.transpose(A.data, (2,0,1)))
#
#tiff.imsave("file1.tif", np.transpose(A.contour, (2,0,1)))
#tiff.imsave("file2.tif", np.transpose(A.data, (2,0,1)))


#def circle_levelset(shape, center, sqradius, scalerow=1.0):
#    """Build a binary function with a circle as the 0.5-levelset."""
#    grid = np.mgrid[list(map(slice, shape))].T - center
#    phi = sqradius - np.sqrt(np.sum((grid.T)**2, 0))
#    u = np.float_(phi > 0)
#    return u
#
#macwe = ms.MorphACWE(data, smoothing=1, lambda1=1, lambda2=2)
#macwe.levelset = circle_levelset(data.shape, (30, 50, 80), 25)
#
#ms.evolve_visual3d(macwe, num_iters=1)
#macwe.run(1)



