#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 20 13:34:29 2018

@author: henrikahl
"""

import os
import numpy as np
import tifffile as tiff
from timagetk.components import SpatialImage, imread, imsave
from timagetk.plugins import registration

def read_SpatialImage(fname, dtype=np.uint16):
  sfile_ = imread(fname)
  meta   = sfile_.get_metadata()
  ar = np.array(sfile_.get_array(), dtype=dtype)
  ar = np.reshape(ar, ar.shape + (1,) * (3 - len(ar.shape))) # make 3d
  sfile_ = SpatialImage(ar)

  sfile_.get_metadata()['voxelsize'] = meta['voxelsize']

  return sfile_

def correct_dim(sp_img):
  if len(sp_img.shape) == 2:
    sp_img = np.reshape(sp_img, sp_img.shape + (1,) * (3 - len(sp_img.shape)))
  sp_img = SpatialImage(sp_img)
  return sp_img

def project_intensity(sp_img, dim, fct=np.max):
  return fct(sp_img, axis=dim)

def intensity_projection_stack_single(infiles, dim,
                                      method='rigid_registration', fct=np.max,
                                      LUT='Fire', show=False, outfile=""):
  images = map(read_SpatialImage, infiles)
  mimg   = [project_intensity(ii, dim=dim, fct=fct) for ii in images]
  mimg   = map(SpatialImage, mimg)
  mimg   = map(correct_dim,  mimg)

  for ii in xrange(1, len(mimg)):
    _, mimg[ii] = registration(mimg[ii], mimg[ii - 1],
                               method=method)

  mimg = np.concatenate(mimg, axis=2)

  if show:
    tiff.imshow(np.transpose(mimg, (2,0,1)))
  if outfile != "":
    imsave(outfile, mimg)

  return mimg

def intensity_projection_stack_all(infiles, method='rigid_registration',
                                   fct=np.max, LUT='Fire', show=False,
                                   outfile="", order='h'):
  c1 = intensity_projection_stack_single(infiles, 2, method=method, fct=fct,
                                         LUT=LUT, show=False, outfile="")
  c2 = intensity_projection_stack_single(infiles, 0, method=method, fct=fct,
                                         LUT=LUT, show=False, outfile="")
  c3 = intensity_projection_stack_single(infiles, 1, method=method, fct=fct,
                                         LUT=LUT, show=False, outfile="")

  import matplotlib.pyplot as plt
  fig = plt.figure()
  if order == "v":
    tiff.imshow(np.transpose(c1, (2,0,1)), subplot=311, figure=fig)
    tiff.imshow(np.transpose(c2, (2,0,1)), subplot=312, figure=fig)
    tiff.imshow(np.transpose(c3, (2,0,1)), subplot=313, figure=fig)
  else:
    tiff.imshow(np.transpose(c1, (2,0,1)), subplot=131, figure=fig)
    tiff.imshow(np.transpose(c2, (2,0,1)), subplot=132, figure=fig)
    tiff.imshow(np.transpose(c3, (2,0,1)), subplot=133, figure=fig)

def intensity_projection_series_single(infiles, dim, method='rigid_registration',
                                   fct=np.max, LUT='Fire', show=False,
                                   outfile=""):

  images = map(read_SpatialImage, infiles)
  mimg   = [project_intensity(ii, dim=dim, fct=fct) for ii in images]
  mimg   = map(SpatialImage, mimg)
  mimg   = map(correct_dim,  mimg)

  for ii in xrange(1, len(mimg)):
    _, mimg[ii] = registration(mimg[ii], mimg[ii - 1],
                               method=method)

  dx = (mimg[0].shape[0])
  output = np.zeros((len(mimg) * dx, mimg[0].shape[1], 1))
  for ii in xrange(len(mimg)):
    output[(ii * dx):((ii + 1) * dx)] = mimg[ii]

  if show:
    tiff.imshow(np.transpose(output, (1,0,2)))
  if outfile != "":
    imsave(outfile, output)

def intensity_projection_series_all(infiles, dim, method='rigid_registration',
                                   fct=np.max, LUT='Fire', show=False,
                                   outfile=""):
  c1 = intensity_projection_series_single(infiles, 2, method=method, fct=fct,
                                         LUT=LUT, show=False, outfile="")
  c2 = intensity_projection_series_single(infiles, 0, method=method, fct=fct,
                                         LUT=LUT, show=False, outfile="")
  c3 = intensity_projection_series_single(infiles, 1, method=method, fct=fct,
                                         LUT=LUT, show=False, outfile="")
  c1, c2, c3 = np.transpose(c1), np.transpose(c2), np.transpose(c3)

  dx = (c1[0].shape[0])
  dy = (c1[0].shape[1], c2[0].shape[1], c3[0].shape[1])
  output = np.zeros((len(c1) * dx, np.sum(dy), 1))

  for ii in xrange(len(c1)):
    output[(ii * dx):((ii + 1) * dx), (dy[0]):(dy[1])] = c1[ii]
    output[(ii * dx):((ii + 1) * dx), (dy[1]):(dy[1] + dy[2])] = c2[ii]
    output[(ii * dx):((ii + 1) * dx), (dy[1] + dy[2]):(dy[0] + dy[1] + dy[2])] = c3[ii]


  if outfile != "":
    imsave(outfile, output)

################################################################################
################################################################################
################################################################################

#inpath = "/home/henrikahl/output-off_NPA/mips"
#inpath = "/home/henrikahl/deconv3"
inpath = "/home/henrikahl/conv-1"

files = os.listdir(inpath)
files = map(lambda x: os.path.join(inpath, x), files)
files = filter(lambda x: not '-5um' in x, files)
files.sort()
c1 = filter(lambda x: 'C1' in x, files)

plant3 = filter(lambda x: "off_NPA-4" in x, c1)
images = [read_si(ii) for ii in plant3]

