#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  6 11:04:59 2018

@author: henrik
"""

import vtk
import pandas as pd
import tifffile as tiff
from scipy.ndimage.morphology import binary_fill_holes
from skimage.exposure import equalize_hist, equalize_adapthist
import phenotastic.plot as pl
import os
import numpy as np
import phenotastic.Meristem_Phenotyper_3D as ap
import copy
from skimage import measure
from vtk.util import numpy_support as nps
#from scipy.ndimage.morphology import binary_opening, binary_closing, binary_dilation, binary_erosion
from scipy.ndimage.filters import gaussian_filter, median_filter
from skimage.segmentation import morphological_chan_vese
import vtkInterface as vi
from vtkInterface.common import AxisRotation
import phenotastic.domain_processing as boa
import phenotastic.mesh_processing as mp
import phenotastic.file_processing as fp
from phenotastic.external.clahe import clahe
from phenotastic.misc import mkdir, listdir, autocrop

home = os.path.expanduser('~')
dir_ = '/home/henrik/data/from-marcus/Ad ab volumes'

files = listdir(dir_, include='.tif')
files.sort()

fname = files[0]
f = fp.tiffload(fname)
res = fp.get_resolution(f)

f.data = autocrop(f.data, threshold=0, fct=np.max)

from stackAlign.deconv import create_psf, deconvolve
import gc

# u'HardwareSetting|FilterSettingRecord|Laser wavelength #1 = 405',
# u'HardwareSetting|FilterSettingRecord|Laser wavelength #2 = 458',
# u'HardwareSetting|FilterSettingRecord|Numerical aperture (Obj.) #1 = 0.95',
# u'HardwareSetting|FilterSettingRecord|Objective #1 = HCX IRAPO L  25.0x0.95 WATER ',
# u'HardwareSetting|FilterSettingRecord|Polarization FW #1 = NF 488/561/633',
# u'HardwareSetting|FilterSettingRecord|SP Mirror Channel 1 (left) #1 = 401',
# u'HardwareSetting|FilterSettingRecord|SP Mirror Channel 1 (right) #1 = 443',
# u'HardwareSetting|FilterSettingRecord|SP Mirror Channel 2 (left) #1 = 564.480519480519',
# u'HardwareSetting|FilterSettingRecord|SP Mirror Channel 2 (right) #1 = 615.909090909091',
# u'HardwareSetting|FilterSettingRecord|SP Mirror Channel 3 (left) #1 = 726.753246753247',
# u'HardwareSetting|FilterSettingRecord|SP Mirror Channel 3 (right) #1 = 751.688311688312',
# u'HardwareSetting|FilterSettingRecord|SP Mirror Channel 4 (left) #1 = 751.688311688312',
# u'HardwareSetting|FilterSettingRecord|SP Mirror Channel 4 (right) #1 = 784.415584415584',
# u'HardwareSetting|FilterSettingRecord|SP Mirror Channel 5 (left) #1 = 784.415584415584',
# u'HardwareSetting|FilterSettingRecord|SP Mirror Channel 5 (right) #1 = 800',
# u'HardwareSetting|FilterSettingRecord|UV Lens FW #1 = Lens 20x/0.70 ',
# u'HardwareSetting|LDM_Block_Sequential|ATLConfocalSettingDefinition|Pinhole = 1.16823512539185E-04',
# u'HardwareSetting|LDM_Block_Sequential|ATLConfocalSettingDefinition|PinholeAiry = 2.0912443360133',
# u'HardwareSetting|LDM_Block_Sequential|ATLConfocalSettingDefinition|Quantity|Unit = m',

MAGNIFICATION = 25
NA = 0.95

iterations = [9]
nchannels = len(iterations)

iopair = (fname, fname[:-4] + '_deconv.tif')

fin = iopair[0]
fout = iopair[1]
print('Now deconvolving ' + str(iopair[0]))

zshape = f.data.shape[0] / 2. + 2.
rshape =  f.data.shape[-1] / 2. + 2.

zdims = zshape * res[0]
rdims = rshape * res[1]

PIN = 488, (492, 516)
DII = 514, (519, 535)
mDII = 561, (566, 590)

ex_wavelens = [488, 514, 561]
em_wavelens = [np.mean([492, 516]), np.mean([519, 535]), np.mean([566, 590])]
pinhole_radii = [116.823512539185 / 2. / MAGNIFICATION]*3

ex_wavelens = [ex_wavelens[1]]
em_wavelens = [em_wavelens[1]]
pinhole_radii = [pinhole_radii[1]]

f.data = f.data[:, [1]]
assert(len(iterations) == len(em_wavelens) == len(ex_wavelens) == f.data.shape[1])

for ii in xrange(nchannels):
    gc.collect()
    cpsf = create_psf(zshape, rshape, zdims, rdims, ex_wavelen=ex_wavelens[ii],
                      em_wavelen=em_wavelens[ii], pinhole_radius=pinhole_radii[ii],
                      num_aperture=NA, magnification=MAGNIFICATION, pinhole_shape='round')
    f.data[:, ii] = deconvolve(f.data[:, ii], psf_vol=cpsf, iterations=iterations[ii], threshold=150)

f.data[f.data > np.iinfo(np.uint16).max] = np.iinfo(np.uint16).max
f.data = f.data.astype(np.uint16)

#fp.tiffsave('1_deconv_test.tif', data=f.data, metadata={'spacing':fp.get_resolution(f)[0]}, resolution=fp.get_resolution(f))

#a = fp.tiffload('1_deconv_test.tif')
