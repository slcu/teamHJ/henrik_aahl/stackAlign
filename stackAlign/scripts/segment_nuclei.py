#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 30 10:45:22 2018

@author: henrik
"""


#rapid_file = '/home/henrik/180312-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-6h/off_NPA/pWUS-3XVENUS-pCLV3-mCherry-off_NPA-1-12h-2.0-750-0.5-770-5um.tif'
#slow_file = '/home/henrik/180312-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-6h/off_NPA/pWUS-3XVENUS-pCLV3-mCherry-off_NPA-1-12h-2.0-750-0.5-770.tif'
dir_ = "/home/henrikahl/tiffs/"
rapid_file = dir_ + 'pWUS-3XVENUS-pCLV3-mCherry-off_NPA-1-12h-2.0-750-0.5-770-5um.tif'
slow_file = dir_ + 'pWUS-3XVENUS-pCLV3-mCherry-off_NPA-1-12h-2.0-750-0.5-770.tif'




#def get_gradient(im) :
#    # Calculate the x and y gradients using Sobel operator
#    grad_x = cv2.Sobel(im,cv2.CV_32F,1,0,ksize=3)
#    grad_y = cv2.Sobel(im,cv2.CV_32F,0,1,ksize=3)
#
#    # Combine the two gradients
#    grad = cv2.addWeighted(np.absolute(grad_x), 0.5, np.absolute(grad_y), 0.5, 0)
#    return grad
#
#
#def mkdir(path):
#  if not os.path.exists(path):
#      os.makedirs(path)
#
#if_default  = '/home/henrikahl/home3/microscopy/teamhj/Henrik-Aahl/180312-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-6h/off_NPA'
#of_default  = os.getcwd() + 'stackAlign_output'
#saf_default = '/home/henrikahl/projects/stackAlign'
#rid_default = '-5um'
#r_default   = [515, 545, 580, 633, 654, 735]
#di_default  = [10, 8, 3]
#
#parser = argparse.ArgumentParser(description='')
#parser.add_argument('input_folder',        type=str, default=if_default)
#parser.add_argument('output_folder',       type=str, default=of_default)
#parser.add_argument('--stackalign_folder', type=str, default=saf_default)
#parser.add_argument('--rapid_id',          type=str, default=rid_default)
#parser.add_argument('--nproc',             type=int, default=1)
#parser.add_argument('--range',             default=r_default, action='append', nargs=2)
#parser.add_argument('--deconv_iters',      default=di_default, nargs='+')
#
#args = parser.parse_args()
#################################################################################
#################################  SETUP  #######################################
#################################################################################
#''' PARAMETERS '''
#INPUT_PATH      = args.input_folder
#OUTPUT_PATH     = args.output_folder
#STACKALIGN_PATH = args.stackalign_folder
#RAPID_ID        = args.rapid_id
#NPROCESSORS     = args.nproc
#DECONV_ITERS    = args.deconv_iters
#
#RANGES      = np.reshape(args.range, (len(args.range) / 2, 2))
#WAVELENGTHS = np.mean(RANGES, axis=1)
#
#''' DIRECTORIES AND FILES '''
#STRETCH_FILE    = os.path.join(OUTPUT_PATH, 'stretching.txt')
#ALIGNMENT_FILE  = os.path.join(OUTPUT_PATH, 'alignment.txt')
#TIFFS_PATH      = os.path.join(OUTPUT_PATH, 'tiffs')
#DECONV_PATH     = os.path.join(OUTPUT_PATH, 'deconv')
#MIPS_PATH       = os.path.join(OUTPUT_PATH, 'mips')
#PSF_PATH        = os.path.join(OUTPUT_PATH, 'PSFs')
#PSF_CONFIG_PATH = os.path.join(PSF_PATH,    'configs')
#
#''' SOFTWARE '''
#DECONVOLUTIONLAB_PATH = os.path.join(STACKALIGN_PATH, 'jars', 'DeconvolutionLab_2.jar')
#PSF_GENERATOR_PATH    = os.path.join(STACKALIGN_PATH, 'jars', 'PSFGenerator.jar')
#
#################################################################################
###############################  RUN STUFF  #####################################
#################################################################################
#''' Create output directories'''
#print('Creating output directories...')
#mkdir(OUTPUT_PATH)
#mkdir(TIFFS_PATH)
#mkdir(PSF_PATH)
#mkdir(PSF_CONFIG_PATH)
#mkdir(DECONV_PATH)
#mkdir(MIPS_PATH)
#for ii in xrange((len(WAVELENGTHS))):
#  mkdir(os.path.join(OUTPUT_PATH, 'channel%d' % (ii + 1)))
#
#''' Convert files to tiff format '''
#
#print('Converting to tiffs...')
#lfiles = os.listdir(INPUT_PATH)
#lfiles = [x for x in lfiles if x.endswith('.lsm')]
#lfiles = map(lambda x: os.path.join(INPUT_PATH, x), lfiles)
#
#p = multiprocessing.Pool(NPROCESSORS)
#p.map(functools.partial(ip.lsm_to_tiff, output_dir=TIFFS_PATH), lfiles)
#p.close(); p.join(); del p
#
#### Identify (tiff) files
#files      = os.listdir(TIFFS_PATH)
#files      = map(lambda x: os.path.join(TIFFS_PATH, x), files)
#files      = [x for x in files if x.endswith('.tif') or x.endswith('.tiff')]
#files      = np.sort(files)
#slowFiles  = [x for x in files if RAPID_ID not in x]
#rapidFiles = np.setdiff1d(files, slowFiles)
#
#''' Calculate the stretching factors '''
##print('Calculating stretch factors...')
##z_correction(TIFFS_PATH, STRETCH_FILE, pruneBottom=2, pruneTop=3,
##             printAlignment=True, alignment_file = ALIGNMENT_FILE)
#
#''' Split channels '''
#def split_channels(fname):
#  fnamebase = os.path.basename(fname)
#  file_     = tiff.TiffFile(fname)
#  data      = file_.asarray()
#  metadata  = file_.imagej_metadata
#
#  # Change important metadata
#  # TODO: Change all metadata values
#  metadata['images'] /= metadata['channels']
#  metadata['channels'] = 1
#
#  for ii in xrange(data.shape[1]): # NOTE: matrix might not be ordered like this
#    outpath = os.path.join(OUTPUT_PATH,
#                           'channel%d' % (ii + 1),
#                           'C%d_%s' % (ii + 1, fnamebase))
#    tiff.imsave(outpath, data=np.array(data[:, ii, ...], dtype='uint16'),
#                imagej=True, metadata=metadata)
#
#print('Splitting channels...')
#p = multiprocessing.Pool(NPROCESSORS)
#p.map(split_channels, files)
#p.close(); p.join(); del p
#
#''' Make PSF config files '''
#print('Creating PSF configuration files...')
#for fname in files:
#  metadata = tiff.TiffFile(fname).imagej_metadata
#  for jj in xrange(len(WAVELENGTHS)):
#    psf_name = os.path.join(
#        PSF_CONFIG_PATH,
#        'PSF-BW_C%d_%s_config.txt' %
#        (jj + 1, os.path.splitext(os.path.basename(fname))[0]))
#    ip.generate_PSF_config(fname=psf_name,
#                           method='BW',
#                           NX=metadata['dimensionx'],
#                           NY=metadata['dimensiony'],
#                           NZ=metadata['dimensionz'],
#                           NA=1.0,
#                           wavelength=WAVELENGTHS[jj],
#                           xystep=metadata['voxelsizex']*1e9,
#                           zstep=metadata['voxelsizez']*1e9,
#                           refr_ind_immersion=1.33)
#
#''' Make actual PSF files '''
#print('Creating PSF files...')
#count = 0
#for config_file in os.listdir(PSF_CONFIG_PATH):
#  if count % 10 == 0:
#    print('\t%.0f %% finished' % (count / len(files)))
#  count += 1
#  out = subprocess.check_output(["java", "-cp", PSF_GENERATOR_PATH,
#                                 "PSFGenerator", os.path.join(PSF_CONFIG_PATH,
#                                                              config_file)])
#  os.rename("PSF BW.tif",
#            os.path.join(PSF_PATH, config_file.split('_config')[0] + '.tif')) # TODO: Make this automatic / parallelisable
#
#''' Deconvolve the files '''
#def deconvolve(fname):
#  fnamebase = os.path.basename(fname)
#  psf_file_path = os.path.join(PSF_PATH, 'PSF-BW_%s' % fnamebase) # TODO: Allow for other methods
#  mip_file_path = os.path.join('mips', os.path.splitext(fnamebase)[0] + '_mip')
#  deconv_file_path = os.path.join('deconv', os.path.splitext(fnamebase)[0] + '_deconv')
#
#  if 'C1' in fnamebase:
#    wl = DECONV_ITERS[0]
#  elif 'C2' in fnamebase:
#    wl = DECONV_ITERS[1]
#  elif 'C3' in fnamebase:
#    wl = DECONV_ITERS[2]
#
#  subprocess.check_call(
#      ['java',       '-jar',  DECONVOLUTIONLAB_PATH, 'Run',
#       '-image',     'file',  fname,
#       '-psf',       'file',  psf_file_path,
#       '-algorithm', 'RL',    str(wl),
#       '-display',   'no',
#       '-out',       'stack', deconv_file_path,      'noshow',
#       '-out',       'mip',   mip_file_path,         'noshow',
#       '-monitor',   'console',
#       '-stats',     'no',
#       '-path',      OUTPUT_PATH
#       ])
#
#print('Deconvolving files...')
#ch1 = os.listdir(OUTPUT_PATH + '/channel1')
#ch2 = os.listdir(OUTPUT_PATH + '/channel2')
#ch3 = os.listdir(OUTPUT_PATH + '/channel3')
#ch1 = map(lambda x: os.path.join(OUTPUT_PATH, "channel1", x), ch1)
#ch2 = map(lambda x: os.path.join(OUTPUT_PATH, "channel2", x), ch2)
#ch3 = map(lambda x: os.path.join(OUTPUT_PATH, "channel3", x), ch3)
#
#all_files_channels = ch1 + ch2 + ch3
#all_files_channels.sort()
#
## Filter already done
#all_files_channels = filter(lambda x:
#  not os.path.isfile(
#      os.path.join(
#          OUTPUT_PATH,
#          'deconv',
#          os.path.splitext(os.path.basename(x))[0] + '_deconv.tif')),
#      all_files_channels)
#
#p = multiprocessing.Pool(NPROCESSORS)
#p.map(deconvolve, all_files_channels)
#p.close(); p.join(); del p

#from timagetk.components import imread, SpatialImage
#DECONV_PATH = os.path.join(OUTPUT_PATH, 'deconv')
#dfiles = os.listdir(DECONV_PATH)
#dfiles = filter(lambda x: x.endswith('.tif') or x.endswith('.tiff'), dfiles)
#dfiles = map(lambda x: os.path.join(DECONV_PATH, x), dfiles)

''' Segment stuff '''
import os
os.chdir('/home/henrik/projects/stackAlign')
import imageProc as ip
import tifffile as tiff
#import subprocess
import numpy as np
#import multiprocessing
#from scipy.ndimage.filters import gaussian_filter
#import cv2

#from timagetk.util import data_path
#from timagetk.plugins import registration
from timagetk.plugins import linear_filtering, h_transform, region_labeling, segmentation
from timagetk.components import SpatialImage, imread
from timagetk.plugins import morphology
from timagetk.plugins import labels_post_processing

dir_ = '/home/henrikahl/deconv4/deconv/'
dir_ = '/home/henrik/off_deconv2/deconv'

dfiles = os.listdir(dir_)
dfiles = map(lambda x: os.path.join(dir_, x), dfiles)
dfiles = filter(lambda x: '5um' not in x, dfiles)
dfiles = filter(lambda x: x.endswith('.tif'), dfiles)
dfiles = filter(lambda x: 'C1_' in x, dfiles)

sp_img = imread(dfiles[0])
sp_img.set_voxelsize([.2516, .2516, .25])
ar = np.array(sp_img.get_array(), dtype='int16')
ar[ar<0] = np.iinfo('int16').max
#tiff.imshow(ar)

'''Do background extraction'''
import background_extraction as bg
contour = bg.contour_fit_threshold(ar, threshold = 2, smooth_iterate=10)
ar[contour] = 0
contour = ~contour
sp_img = SpatialImage(ar, voxelsize=[.2561,.2561,.25])

smooth_img = linear_filtering(sp_img, std_dev=2.0,
                              method='gaussian_smoothing', iterations=2)

smooth_img = morphology(smooth_img, max_radius=2, method='oco_alternate_sequential_filter')
#smooth_img[smooth_img<np.mean(smooth_img)] = 0
from scipy import ndimage as ndi
image_max = ndi.maximum_filter(smooth_img, size=10, mode='constant')
from skimage import morphology as morph
#distance = ndi.distance_transform_edt(contour)

from skimage.feature import peak_local_max
#fp = np.ones((10, 10, 10))
#
#fp[..., :] = fp[..., :]*0.25/0.2561
#local_maxi = peak_local_max(image_max, labels=contour,
#                            footprint=fp,
#                            indices=False)
#markers = ndi.label(local_maxi)[0]
#from skimage.morphology import extrema
#from skimage import exposure
##h = 0.05
#h_maxima = extrema.h_maxima(smooth_img, h)
#markers = ndi.label(h_maxima)[0]
#tiff.imshow(np.transpose(markers, (2,0,1)))
regext_image = h_transform(smooth_img, h=150, method='h_transform_max')
seeds_image = region_labeling(regext_image, low_threshold=1, high_threshold=150,
                                  method='connected_components')
markers = seeds_image
labels = morph.watershed(-smooth_img, markers=markers, mask=contour, connectivity=1, watershed_line=True, compactness = 0)
from skimage import segmentation as segm
print len(np.unique(labels))
labels = morph.remove_small_objects(labels, 20, connectivity=1)
print len(np.unique(labels))


#
import matplotlib.pyplot as plt
tiff.imshow(np.transpose(labels, (2,1,0)), cmap =plt.get_cmap('tab20'))
tiff.imsave('labels.tif', np.transpose(labels, (2,1,0)))
#from skimage import data, util, filters, color
#
#import copy
#imc = copy.deepcopy(smooth_img)
#imc = np.array(imc, dtype='float')
##for ii in xrange(imc.shape[2]):
##  imc[..., ii] = filters.sobel(imc[..., ii])
#from scipy.ndimage import sobel, generic_gradient_magnitude
#imc = generic_gradient_magnitude(imc, sobel)
#
#
#fp = np.ones((10, 10, 10))
#fp[..., :] = fp[..., :]*0.25/0.2561
#local_maxi = peak_local_max(-imc, labels=contour,
#                            footprint=fp,
#                            indices=False)
#markers = ndi.label(local_maxi)[0]
#labels = morph.watershed(-smooth_img, markers=markers, mask=contour, connectivity=1, watershed_line=True, compactness = 0)
#print len(np.unique(labels))
#labels = morph.remove_small_objects(labels, 20, connectivity=1)
#print len(np.unique(labels))
#tiff.imshow(np.transpose(labels, (2,1,0)))

#tiff.imshow(np.transpose(imc, (2,1,0)))

#from skimage.viewer import ImageViewer

################################################################################



#segm = morphology.watershed(smooth_img, seeds_img, mask=contour, watershed_line=False)
#tiff.imshow(np.transpose(segm, (2,0,1)))

#tiff.imshow(np.transpose(smooth_img, (2,0,1)))
#tiff.imshow(np.transpose(asf_img, (2,0,1)))
#tiff.imshow(np.transpose(seeds_img, (2,0,1)))

#tiff.imread(fname).shape
#
#import timagetk.util
##from timagetk.util import data, data_path
##def invert_image(img):
##  return np.finfo(img.dtype).max - img
#
##tiff.imshow(np.transpose(inv_img, (2,0,1)))
#
#input_image = sp_image
#smooth_image = linear_filtering(input_image, std_dev=2.0, method='gaussian_smoothing')
#regext_image = h_transform(smooth_image, h=5, method='h_transform_min')
#
#seeds_image = region_labeling(smooth_image, low_threshold=1, high_threshold=3,
#                                  method='connected_components')
#tiff.imshow(smooth_image.transpose(2,0,1))
#tiff.imshow(regext_image.transpose(2,0,1))
#tiff.imshow(seeds_image.transpose(2,0,1))
#####
#

#
#tfiles = os.listdir(OUTPUT_PATH + "/channel1")
#tfiles = map(lambda x: OUTPUT_PATH + "/channel1/" + x, tfiles)
#
#sp_image = imread(tfiles[0])
#sp_image = imread(dfiles[3])
#
#meta = sp_image.get_metadata()
#
##sp_image = SpatialImage(np.array(np.invert(np.array(sp_image, dtype='uint16')), np.float32))
#sp_image =  SpatialImage(np.iinfo(np.array(sp_image, dtype='uint16').dtype).max - np.array(sp_image, dtype='uint16'))
##sp_image =  SpatialImage(np.finfo(np.array(sp_image).dtype).max - np.array(sp_image))
#
##sp_image
#sp_image.set_metadata(meta)
#input_img = sp_image
#input_img.set_voxelsize([.2516, .2516, .250])
#
#smooth_img = linear_filtering(input_img, std_dev=2.0,
#                              method='gaussian_smoothing')
#asf_img = morphology(smooth_img, max_radius=5,
#                     method='co_alternate_sequential_filter')
## hysteresis
#ext_img = h_transform(asf_img, h=300,
#                      method='h_transform_min')
#
## Might not be working correctly
## Look at scikit-image::connected_components -- extract array & metadata
#con_img = region_labeling(ext_img, low_threshold=1,
#                          high_threshold=300,
#                          method='connected_components')
#
#seg_img = segmentation(smooth_img, con_img,
#                       method='seeded_watershed')
#
#pp_img1 = labels_post_processing(seg_img, radius=1,
#                                iterations=1,
#                                method='labels_erosion')
#pp_img2 = labels_post_processing(pp_img1, radius=10,
#                                iterations=10,
#                                method='labels_dilation')
#pp_img3 = labels_post_processing(pp_img2, radius=10,
#                                iterations=10,
#                                method='labels_opening')
#pp_img4 = labels_post_processing(pp_img3, radius=10,
#                                iterations=10,
#                                method='labels_closing')
#
#tiff.imshow(np.array(pp_img1.transpose(2,0,1)))
#tiff.imshow(np.array(pp_img2.transpose(2,0,1)))
#tiff.imshow(np.array(pp_img3.transpose(2,0,1)))
#tiff.imshow(np.array(pp_img4.transpose(2,0,1)))
#
#
#
#tiff.imshow(np.array(input_img.transpose(2,0,1)))
#tiff.imshow(np.array(asf_img.transpose(2,0,1)))
#tiff.imshow(np.array(ext_img.transpose(2,0,1)))
#tiff.imshow(np.sum(np.array(con_img.transpose(2,0,1)), axis=0))
#
#tiff.imshow(np.array(con_img.transpose(2,0,1)))
#tiff.imshow(np.array(seg_img.transpose(2,0,1)))
##tiff.imshow(np.array(pp_img.transpose(2,0,1)))
