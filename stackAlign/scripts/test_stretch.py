#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 21 14:37:33 2018

@author: henrik
"""
import functools
import argparse
import os
import multiprocessing
from stackAlign.misc import mkdir
import numpy as np
import stackAlign.imageProc as ip
import subprocess
import stackAlign.background_extraction as bg
import tifffile as tiff
import copy
from stackAlign.alignment import z_correct_dir
import pandas as pd
import phenotastic.file_processing as fp
from skimage import restoration
import scipy
import gc

#if_default  = '/home/henrikahl/home3/microscopy/teamhj/Henrik-Aahl/180312-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-6h/off_NPA'
#of_default  = os.getcwd() + 'stackAlign_output'
#saf_default = '/home/henrikahl/projects/stackAlign'
#rid_default = '-5um'
#refid_default = 'reference'
#r_default   = [515, 545, 580, 633, 654, 735]
#di_default  = [10, 8, 3]
##exit()
#parser = argparse.ArgumentParser(description='')
#parser.add_argument('input_folder',        type=str, default=if_default)
#parser.add_argument('output_folder',       type=str, default=of_default)
#parser.add_argument('--stackalign_folder', type=str, default=saf_default)
#parser.add_argument('--rapid_id',          type=str, default=rid_default)
#parser.add_argument('--reference_id',      type=str, default=refid_default)
#parser.add_argument('--nproc',             type=int, default=1)
#
#args = parser.parse_args()


################################################################################
#INPUT_PATH      = args.input_folder
#OUTPUT_PATH     = args.output_folder
##STACKALIGN_PATH = args.stackalign_folder
#RAPID_ID        = args.rapid_id
#NPROCESSORS     = args.nproc
#REFERENCE_ID    = args.reference_id
#STACKALIGN_PATH = os.path.abspath('')

INPUT_PATH = '/home/henrik/data/teststretch'
OUTPUT_PATH='/home/henrik/test'
RAPID_ID = '-5um'
REFERENCE_ID = 'reference'

ALIGNMENT_PRUNE_BOTTOM = 0
ALIGNMENT_PRUNE_TOP = 0
ALIGNMENT_GAUSSIAN_FILTER = 0
ALIGNMENT_METHOD = 'rigid'

''' DIRECTORIES AND FILES '''
STRETCH_FILE    = os.path.join(OUTPUT_PATH, 'stretching.txt')
ALIGNMENT_FILE  = os.path.join(OUTPUT_PATH, 'alignment.txt')
TIFFS_PATH      = os.path.join(OUTPUT_PATH, 'tiffs')
CROP_PATH       = os.path.join(OUTPUT_PATH, 'cropped')
DECONV_PATH     = os.path.join(OUTPUT_PATH, 'deconv')
FILTERED_PATH   = os.path.join(OUTPUT_PATH, 'filtered')
MIPS_PATH       = os.path.join(OUTPUT_PATH, 'mips')
PSF_PATH        = os.path.join(OUTPUT_PATH, 'PSFs')
PSF_CONFIG_PATH = os.path.join(PSF_PATH,    'configs')

''' SOFTWARE '''
################################################################################
##############################  RUN STUFF  #####################################
################################################################################
''' Create output directories '''
print('Creating output directories...')
mkdir(OUTPUT_PATH)
mkdir(DECONV_PATH)

for ii in xrange((3)):
    mkdir(os.path.join(OUTPUT_PATH, 'C%d' % (ii + 1)))

################################################################################
'''' Converting to tiffs '''
NPROCESSORS = 1
p = multiprocessing.Pool(NPROCESSORS)

################################################################################
def listdir(path):
    files = os.listdir(path)
    files = map(lambda x: os.path.join(path, x), files)
    return files

#files = listdir(INPUT_PATH)
#files = listdir('/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h_STACKREG/')
#files.sort()
#files = files[:2]
#files = files[1]
#
#f = fp.tiffload(files)
#data = f.data[-50:-40, 0]
##data = data[:,200:400, 200:400]
#metadata = f.metadata
#zshape = data.shape[0] / 2. + 2.
#rshape =  data.shape[-1] / 2. + 2.
#
#zdims = zshape * metadata['spacing']
#rdims = rshape * metadata['voxelsizex']
#
#lasers = np.array([x['wavelength'] for x in eval(metadata['scaninformation'])['tracks'][0]['illuminationchannels']], dtype='int')
#lasers = np.sort(lasers)
#
#em_wavelens = np.array([(x['spiwavelengthstart'], x['spiwavelengthstop']) for x in eval(metadata['scaninformation'])['tracks'][0]['detectionchannels']])
#ex_wavelens = np.array([max(lasers[lasers < x[0]]) for x in em_wavelens])
#em_wavelens = np.mean(em_wavelens, axis=1)

#import stackAlign.external.psf
from stackAlign.external import psf

def create_psf(zshape, rshape, zdims, rdims, ex_wavelen, em_wavelen, pinhole_radius,
               num_aperture=1.0, refr_index=1.333,  pinhole_shape='square',
               psf_type=(psf.ISOTROPIC | psf.CONFOCAL), magnification=20.0):
    args = dict(shape=(int(zshape), int(rshape)), dims=(zdims, rdims), ex_wavelen=ex_wavelen,
                em_wavelen=em_wavelen, num_aperture=num_aperture,
                refr_index=refr_index, pinhole_radius=pinhole_radius,
                pinhole_shape=pinhole_shape, magnification=magnification)
    obsvol = psf.PSF(psf_type, **args)

    return obsvol.volume()

def deconvolve(data, psf_vol, iterations, method='LR', bgfact=2.):
    data = copy.deepcopy(data)
    med = np.median(data[0]) * bgfact
    data[data < med] = 0

    if method == 'LR':
        deconv = restoration.richardson_lucy(image=data, psf=psf_vol, iterations=iterations, clip=False)
        gc.collect()

    deconv[deconv < 0] = 0
    deconv = scipy.signal.wiener(deconv, mysize=(3, 3, 3), noise=500000)
    deconv[deconv < 0] = 0

    return deconv

def deconv_file_to_file(fname, iterations=[]):
    MAGNIFICATION = 20
    nchannels = len(iterations)

    f = fp.tiffload(fname)

    metadata = f.metadata
    zshape = f.data.shape[0] / 2. + 2.
    rshape =  f.data.shape[-1] / 2. + 2.

    zdims = zshape * metadata['spacing']
    rdims = rshape * metadata['voxelsizex']

    lasers = np.array([x['wavelength'] for x in eval(metadata['scaninformation'])['tracks'][0]['illuminationchannels']], dtype='int')
    lasers = np.sort(lasers)

    em_wavelens = np.array([(x['spiwavelengthstart'], x['spiwavelengthstop']) for x in eval(metadata['scaninformation'])['tracks'][0]['detectionchannels']])
    ex_wavelens = np.array([max(lasers[lasers < x[0]]) for x in em_wavelens])
    em_wavelens = np.mean(em_wavelens, axis=1)

    pinhole_radii = np.array([eval(metadata['scaninformation'])['tracks'][0]['detectionchannels'][ii]['pinholediameter'] / 2. / MAGNIFICATION for ii in xrange(nchannels)])

    assert(len(iterations) == len(em_wavelens) == len(ex_wavelens) == f.data.shape[1])

    for ii in xrange(nchannels):
        cpsf = create_psf(zshape, rshape, zdims, rdims, ex_wavelen=ex_wavelens[ii],
                          em_wavelen=em_wavelens[ii], pinhole_radius=pinhole_radii[ii])
        f.data[:, ii] = deconvolve(f.data[:, ii], psf_vol=cpsf, iterations=iterations[ii], bgfact=2.0)



