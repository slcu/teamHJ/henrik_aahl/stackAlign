#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 22 17:16:55 2018

@author: henrik
"""
import argparse, os
import functools
import tifffile as tiff
import numpy as np
from misc import mkdir
from analyse import intensity_projection_series_all
from imageProc import split_channels, lsm_to_tiff
import multiprocessing
import re


parser = argparse.ArgumentParser(description='')
parser.add_argument('input_folder', type=str)
parser.add_argument('output_folder', type=str)
parser.add_argument('--nproc', type=int, default=1)
parser.add_argument('--plantid', type=str)

#args = parser.parse_args()

#INPUT_PATH      = "/home/henrik/data/180129-pWUS-3X-VENUS-pCLV3-mCherry-Timelapse/on_NPA"
#OUTPUT_PATH     = os.path.join(os.getcwd(), 'stackAlign_output')
#INPUT_PATH      = args.input_folder#"/home/henrik/data/180129-pWUS-3X-VENUS-pCLV3-mCherry-Timelapse/on_NPA"
#OUTPUT_PATH     = args.output_folder #os.path.join(os.getcwd(), 'stackAlign_output')
#NPROCESSORS     = args.nproc
#PLANT_ID        = args.plantid

OUTPUT_PATH = '/home/henrikahl/newdir2'
#INPUT_PATH = '/home/henrikahl/home3/microscopy/teamhj/Henrik-Aahl/180213-pWUS-3X-VENUS-pCLV3-mCherry-Timelapse'
INPUT_PATH = '/home/henrikahl/home3/microscopy/teamhj/Henrik-Aahl/180312-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-6h/on_NPA'
PLANT_ID = 'on_NPA-2' # remember -
NPROCESSORS = 4
PLANT_REGEX = "..-NPA-\d"
nChannels = 3

TIFFS_PATH      = os.path.join(OUTPUT_PATH, 'tiffs')
p = multiprocessing.Pool(NPROCESSORS)

mkdir(OUTPUT_PATH)
mkdir(TIFFS_PATH)

#'''' Converting to tiffs '''
print('Converting to tiffs...')
lfiles = os.listdir(INPUT_PATH)
lfiles = [x for x in lfiles if x.endswith('.lsm')]
lfiles = filter(lambda x : PLANT_ID in x, lfiles)
lfiles = filter(lambda x : not 'reference' in x and not 'REFERENCE' in x, lfiles)
lfiles = filter(lambda x : not '5um' in x, lfiles)
lfiles = map(lambda x: os.path.join(INPUT_PATH, x), lfiles)
nChannels = tiff.TiffFile(lfiles[0]).lsm_metadata['DimensionChannels']

#p.map(functools.partial(lsm_to_tiff, output_dir=TIFFS_PATH), lfiles)

tfiles      = os.listdir(TIFFS_PATH)
tfiles      = map(lambda x: os.path.join(TIFFS_PATH, x), tfiles)
tfiles      = [x for x in tfiles if x.endswith('.tif') or x.endswith('.tiff')]
tfiles      = np.sort(tfiles)

''' Split channels '''
print('Splitting channels...')
for ii in xrange(3):
  mkdir(os.path.join(OUTPUT_PATH, 'channel%d' % (ii + 1)))

#p.map(functools.partial(split_channels, output_path=OUTPUT_PATH), tfiles)

''' Make mips '''
CH1_PATH = os.path.join(OUTPUT_PATH, 'channel%d' % (0 + 1))
ch1      = os.listdir(CH1_PATH)
ch1      = map(lambda x: os.path.join(CH1_PATH, x), ch1)
ch1      = [x for x in ch1 if x.endswith('.tif') or x.endswith('.tiff')]
ch1      = list(np.sort(ch1))


p = re.compile(PLANT_REGEX)
plants = []
for ii in lfiles:
  for jj in p.finditer(ii):
    plants.append(jj.group())
plants = np.unique(plants)

#for ch in xrange(nChannels):
#  for ii in plants:
#    pfiles = filter(lambda x: ii in x, tfiles)
#    intensity_projection_series_all(pfiles, channel=ch,fct=np.max,
#                                    method='rigid_registration',
#                                    outfile=os.path.join(OUTPUT_PATH, 'C%d-' % (ch + 1)  + ii + '_mips.tif'))

#p.close()
#p.join()
#del p