#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 31 11:24:59 2018

@author: henrik
"""

import functools
import argparse
import os
import multiprocessing
from stackAlign.misc import mkdir
import numpy as np
import stackAlign.imageProc as ip
import subprocess
import stackAlign.background_extraction as bg
import tifffile as tiff
import copy
#from stackAlign.alignment import z_correction
import pandas as pd
import phenotastic.file_processing as fp
from phenotastic.misc import mkdir, listdir, autocrop, remove_empty_slices, match_shape
import stackAlign.external.psf as psf
from skimage import restoration
import gc
import scipy

# 1. Crop away empty slices
# 2. StackReg
# 3. Stretch & correct
# 4. Crop away background
# 5. Deconvolve and remove background noise
# 6. Make MIPS with absolute intensity
# 7. Make MIPS with relative intensity

files = listdir('/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h_STACKREG', include='tif', exclude='-5um')
files.sort()

rfiles = listdir('/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h_STACKREG', include=['-5um.tif'])
rfiles.sort()


fname = files[0]

from pystackreg import StackReg
from skimage.transform import warp

def process(fname):
    ''' CROP AWAY EMPTY SLICES '''
    f = fp.tiffload(fname)
    f.data = remove_empty_slices(f.data)

    ''' STACKREG '''
    channel_mean = np.mean(f.data, axis=1)

    sr = StackReg(StackReg.RIGID_BODY)
    tmats = sr.register_stack(channel_mean, reference='previous')

    for ii in xrange(f.data.shape[0]):
        for jj in xrange(f.data.shape[1]):
            f.data[ii, jj] = warp(f.data[ii, jj], tmats[ii], preserve_range=True)

    ''' AUTOCROP '''
    f.data = autocrop(f.data, threshold=8000, fct=np.max)

    rpath = fname[:-4] + '-5um.tif'
    if os.path.exists(rpath):
        rf = fp.tiffload(fname[:-4] + '-5um.tif')
        rf.data = remove_empty_slices(rf.data)
        rf.data = autocrop(rf.data, threshold=12000, fct=np.max)

        ''' STRETCH & CORRECT '''
        rfile = rf
        sfile = f
        method = 'rigid'
        weight = 'relative_intensity'

        rapidImage = rfile.data
        slowImage = sfile.data

        nrapid = rapidImage.shape[0]
        nslow = slowImage.shape[0]

        sr = StackReg(StackReg.RIGID_BODY)
        rapidImage = np.mean(rapidImage, axis=1)
        slowImage = np.mean(slowImage, axis=1)

        pairs = []
        max_value = np.max(rfile.data[:, 2])
        weights = np.max(rfile.data[:, 2], axis=(1, 2)).astype('float') / max_value

        # Make sure all images same dimension
        new_rapid = []
        for ii in xrange(nrapid):
            new_rapid.append(match_shape(rapidImage[ii], slowImage[0].shape))
        rapidImage = np.array(new_rapid)

        for rindex in xrange(nrapid):
            print('sliceNb = ', rindex)
            new = np.insert(slowImage, 0, rapidImage[rindex], axis=0)
            new = sr.register_transform_stack(new, reference='first')
            distances = np.sum(np.abs(new - new[0]), axis=(1, 2))[1:]
            slice_scores = np.array(zip(distances, [rindex] * nslow, xrange(nslow)))

            pairs.append(slice_scores)

        scores = np.array([np.array(ii) for ii in pairs])
    #    best_diff = np.min([scores[ii][:, 0] for ii in xrange(len(scores))])
        for ii in xrange(nrapid):
            scores[ii] = np.array(scores[ii], dtype='float')
            scores[ii][:, 0] = scores[ii][:, 0] / np.min(scores[ii][:, 0])
    #        scores[ii][:, 0] /= best_diff
            scores[ii][:, 0] = 1. / scores[ii][:, 0]
            if weight == 'relative_intensity':
                scores[ii][:, 0] *= weights[ii]

        from stackAlign.alignment import align

        pairs = scores
        rSpace = rfile.metadata['spacing']
        sSpace = sfile.metadata['spacing']
        falseSliceRatio = float(rSpace) / sSpace

        # TODO: Change this and include information in output file
        missSlowMid = 1. / falseSliceRatio
        missSlowEdge = missSlowMid
        missRapidMid = np.finfo(np.float).min
        missRapidEdge = 1. / falseSliceRatio

        matches = align(pairs, missSlowMid=missSlowMid,
                        missSlowEdge=missSlowEdge, missRapidMid=missRapidMid,
                        missRapidEdge=missRapidEdge, outfile='',
                        printAlignment=False)

        finalPairs = [(int(pa[0]), int(pa[1])) for pa in matches]
        dists = np.diff([ii[1] for ii in finalPairs]) / (rSpace / sSpace)
    #    dists = np.multiply(dists, np.mean(np.array(zip(weights[:-1], weights[1:])), axis=1))
        dists = np.sort(dists)

        meanStretchFactor = np.average(dists, weights=np.mean(
                np.array(zip(weights[:-1], weights[1:])), axis=1)) / falseSliceRatio # weighted mean

        print('mean stretch factor: ' + str(meanStretchFactor))

        meanStretchFactor = max(meanStretchFactor, 1.0)

        f.metadata['spacing'] = f.metadata['spacing'] / meanStretchFactor
        if 'voxelsizez' in f.metadata.keys():
            f.metadata['voxelsizez'] = f.metadata['spacing']
        if 'VoxelsizeZ' in f.metadata.keys():
            f.metadata['VoxelsizeZ'] = f.metadata['spacing']

        with open(fname[:-4] + '_stretch.txt', 'w') as sout:
            sout.write(meanStretchFactor)

    ''' DECONVOLVE '''

    from stackAlign.deconv import create_psf, deconvolve

    def deconv_file_to_file(iopair, iterations=[]):
        import gc
        MAGNIFICATION = 20
        nchannels = len(iterations)
        fin = iopair[0]
        fout = iopair[1]
        print('Now deconvolving ' + str(iopair[0]))

        f = fp.tiffload(fin)
        metadata = f.metadata

        zshape = f.data.shape[0] / 2. + 2.
        rshape =  f.data.shape[-1] / 2. + 2.

        zdims = zshape * metadata['spacing']
        rdims = rshape * metadata['voxelsizex']

        ich = eval(metadata['scaninformation'])['tracks'][0]['illuminationchannels']
        dch = eval(metadata['scaninformation'])['tracks'][0]['detectionchannels']

        lasers = np.array([x['wavelength'] for x in ich], dtype='int')
        lasers = np.sort(lasers)

        em_wavelens = np.array([(x['spiwavelengthstart'], x['spiwavelengthstop']) for x in dch])
        ex_wavelens = np.array([max(lasers[lasers < x[0]]) for x in em_wavelens])
        em_wavelens = np.mean(em_wavelens, axis=1)

        pinhole_radii = np.array([dch[ii]['pinholediameter'] / 2. / MAGNIFICATION for ii in xrange(nchannels)])

        assert(len(iterations) == len(em_wavelens) == len(ex_wavelens) == f.data.shape[1])

        for ii in xrange(nchannels):
            gc.collect()
            cpsf = create_psf(zshape, rshape, zdims, rdims, ex_wavelen=ex_wavelens[ii],
                              em_wavelen=em_wavelens[ii], pinhole_radius=pinhole_radii[ii])
            f.data[:, ii] = deconvolve(f.data[:, ii], psf_vol=cpsf, iterations=iterations[ii], bgfact=2.0)

        f.data[f.data > np.iinfo(np.uint16).max] = np.iinfo(np.uint16).max
        f.data = f.data.astype(np.uint16)

        fp.tiffsave(fout, f.data, metadata=metadata, resolution=[f.metadata['spacing'],
                                                                 f.metadata['voxelsizey'],
                                                                 f.metadata['voxelsizex']])




    dfiles = listdir(outdir)
    dfiles_after = map(lambda x: x[:-4] + '_deconv.tif', dfiles)
    fpairs = zip(dfiles, dfiles_after)

    pool = multiprocessing.Pool(NPROCESSORS)
    pool.map(functools.partial(deconv_file_to_file, iterations=[10, 10, 8]), fpairs)
    pool.close()
    pool.join()





