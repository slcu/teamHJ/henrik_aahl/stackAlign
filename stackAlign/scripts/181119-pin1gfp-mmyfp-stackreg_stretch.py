#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 00:00:17 2018

@author: henrik
"""
import phenotastic.file_processing as fp
from pystackreg import StackReg
from skimage.transform import warp
from phenotastic.misc import listdir, mkdir, autocrop
import numpy as np
import gc
import tifffile as tiff
import os
from stackAlign.deconv import create_psf, deconvolve
import mahotas as mh
from skimage.exposure import rescale_intensity
from multiprocessing import Pool
from phenotastic.misc import intensity_projection_series_all
import re
from joblib import Parallel, delayed
import phenotastic.file_processing as fp
from pystackreg import StackReg
from skimage.transform import warp
from phenotastic.misc import listdir, mkdir, autocrop
import numpy as np
import gc
import tifffile as tiff
import os
from stackAlign.deconv import create_psf, deconvolve
import mahotas as mh
from skimage.exposure import rescale_intensity
from skimage.transform import EuclideanTransform, warp
from scipy.ndimage.measurements import center_of_mass

def run_reg(data):
    channelmax = np.max(data, axis=1)
    channelmax[channelmax < mh.otsu(channelmax.astype('uint16'), ignore_zeros=True)] = 0
    channelmax = rescale_intensity(channelmax, out_range='uint8').astype('uint8')
    
    # Perform stack regularisation
    sr = StackReg(StackReg.RIGID_BODY)
    tmats = sr.register_stack(channelmax, reference='previous')
    for ii in xrange(data.shape[0]):
        for jj in xrange(data.shape[1]):
            data[ii, jj] = warp(data[ii, jj], tmats[ii], preserve_range=True)
    return data

def jbsa(outdir, fname, ext):
    return os.path.join(outdir, os.path.basename(os.path.splitext(fname)[0]) + ext)

def regularise_stack(fname, outdir):
    f = fp.tiffload(fname)

    # Remove empty slices
    not_empty = np.sum(f.data, axis=(1, 2, 3)) > 0
    f.data = f.data[not_empty]
    
    # Re-align stack
    f.data = run_reg(f.data)
    
    # Save intermediate state
    outfile = jbsa(outdir, fname, '_stackreg.tif')
    fp.tiffsave(outfile, f.data,
                resolution=fp.get_resolution(f),
                metadata=f.metadata)

''' Unstretch '''
def align_pair(bigpath, smallpath, outdir, background_threshold=None, autocrop_threshold=None, 
               autocrop_n=None, score_threshold=1., weight='relative_intensity', 
               measure='euclid'):
    from stackAlign.alignment import match_all, align
    from phenotastic.misc import mode

    bigfile = fp.tiffload(bigpath)
    smallfile = fp.tiffload(smallpath)
    
    bigfile.data = bigfile.data[np.sum(bigfile.data, axis=(1, 2, 3)) > 0]
    smallfile.data = smallfile.data[np.sum(smallfile.data, axis=(1, 2, 3)) > 0]

    if background_threshold is not None:
        for ii in xrange(len(background_threshold)):
            bigfile.data[:, ii][bigfile.data[:, ii] < background_threshold[ii]] = 0
            smallfile.data[:, ii][smallfile.data[:, ii] < background_threshold[ii]] = 0

    if autocrop_threshold is not None:        
        bigfile.data = autocrop(bigfile.data, threshold=autocrop_threshold[0], 
                                n=autocrop_n[0])
        smallfile.data = autocrop(smallfile.data, threshold=autocrop_threshold[1], 
                                  n=autocrop_n[1])
    
    try:
        pairs = match_all(bigimage=bigfile.data, smallimage=smallfile.data, weight=None, measure=measure)
    
        # Get costs/scores
        bigspace = fp.get_resolution(smallfile)[0]
        smallspace = fp.get_resolution(bigfile)[0]
        sliceratio = bigspace / smallspace
#        sliceratio = 1. / sliceratio

        miss_big_mid = 1. / sliceratio
        miss_big_edge = 1. / sliceratio
        miss_small_mid = np.finfo(np.float).min # As soon as things start matching, we can't have gaps 
        miss_small_edge = 1. / sliceratio

        # Align    
        matches = align(pairs, miss_big_mid=miss_big_mid, miss_big_edge=miss_big_edge, 
                        miss_small_mid=miss_small_mid, miss_small_edge=miss_small_edge)
    
        scores = np.array([float(pa[2]) for pa in matches])
        final_pairs = [(int(pa[0]), int(pa[1])) for pa in matches]

        dists = np.diff([ii[1] for ii in final_pairs]).astype(float) / sliceratio
        to_remove = np.where(scores < score_threshold)[0]
        to_remove = np.hstack([to_remove, to_remove - 1])
        dists = np.delete(dists, to_remove)
        dists = np.sort(dists)

        # Calculate stretching    
        mean_stretch = np.mean(dists)
        median_stretch = np.median(dists)
        mode_stretch = mode(dists[::-1])
        std_stretch = np.std(dists)
    except:
        mean_stretch = 1.
        median_stretch = 1.
        mode_stretch = 1.
        std_stretch = np.nan
        final_pairs = None
        scores = None
        
    outfile = os.path.join(outdir, 'stretching.txt')
    if not os.path.isfile(outfile):
        with open(outfile, 'w') as f:
            f.write('fname\tmean_stretch\tmedian_stretch\tmode_stretch\tstd_stretch\tfinal_pairs\tscores\n')
    with open(outfile, 'a+') as f:
        fname = jbsa('', bigpath, '')
        f.write('%s\t%f\t%f\t%f\t%f\t%s\t%s\n' % 
                (fname, mean_stretch, median_stretch, mode_stretch, std_stretch, 
                 np.array2string(np.array(final_pairs)).replace('\n', ''), 
                 np.array2string(scores).replace('\n', '')))

def deconvolve_file(fname, outdir, background_threshold=None, autocrop_threshold=None, 
                    autocrop_n=None, iterations=None, magnification=None, NA=None, 
                    ex_wavelens=None, em_wavelens=None, pinhole_radii=None, pinhole_shape='square', 
                    verbose=True):

    if verbose:
        print('Now deconvolving ' + fname)
        
    f = fp.tiffload(fname)
    data = f.data
    metadata = f.metadata
    resolution = fp.get_resolution(f)
    nchannels = data.shape[1]
    del f

    # Check that things are ok
    assert(len(iterations) == len(em_wavelens) == len(ex_wavelens) == data.shape[1] == 
           len(background_threshold) == len(pinhole_radii))
    
    # Crop? Tends to save some space.
    if autocrop_threshold is not None:
        data = autocrop(data, threshold=autocrop_threshold, n=autocrop_n)

    # Spatial info    
    zshape = data.shape[0] / 2. + 2.
    rshape = data.shape[-1] / 2. + 2.
    zdims = zshape * resolution[0]
    rdims = rshape * resolution[1]

    # Loop over channels and perform the deconvolution
    for ii in xrange(nchannels):
        gc.collect()
        cpsf = create_psf(zshape, rshape, zdims, rdims, ex_wavelen=ex_wavelens[ii],
                          em_wavelen=em_wavelens[ii], pinhole_radius=pinhole_radii[ii],
                          num_aperture=NA, magnification=magnification, pinhole_shape=pinhole_shape)
        data[:, ii] = deconvolve(data[:, ii], psf_vol=cpsf, iterations=iterations[ii], 
            threshold=background_threshold[ii])
    
    # Ensure that values are benign
    data[data > np.iinfo(np.uint16).max] = np.iinfo(np.uint16).max
    data = data.astype(np.uint16)

    # Save the file
    cropped = '_cropped' if autocrop_threshold is not None else ''
    outfile = jbsa(outdir, fname, cropped + '_deconvolved.tif')
    fp.tiffsave(outfile, data, resolution=resolution, metadata=metadata)
    
###############################################################################
###############################################################################
''' Run things '''
###############################################################################
###############################################################################

# 1. Correct faulty files
# 2. Regularise stacks
# 3. Find stretching factors
# 4. Actually destretch files
# 5. Deconvolve
# 6. Get maxprojections

outdir = '/home/henrik/what'
mkdir(outdir)

''' 1. CORRECT FAULTY FILES '''
fname = '/media/henrik/Seagate Expansion Drive/181113-PIN-GFP-MM-dsRed-Timelapse-24h/181113-PIN-GFP-MM-dsRed-on_npa-5-72h.lsm'
f = fp.tiffload(fname)
sr = StackReg(StackReg.RIGID_BODY)
data1 = f.data[:187].copy()
data1 = run_reg(data1)

data2 = f.data[187:].copy()
data2 = run_reg(data2)

# Get reference point and combine
c1 = center_of_mass(np.max(data1[-1], axis=0) > mh.otsu(np.max(data1[-1], axis=0)))
c2 = center_of_mass(np.max(data2[0], axis=0) > mh.otsu(np.max(data2[0], axis=0)))
for ii in xrange(data2.shape[0]):
    for jj in xrange(data2.shape[1]):
        data2[ii, jj] = warp(data2[ii, jj], EuclideanTransform(translation=(c2[1] - c1[1], 
             c2[0] - c1[0])), preserve_range=True)
data = np.vstack([data1, data2])
data = run_reg(data)

outfile = jbsa(outdir, fname, '_corrected.tif')
fp.tiffsave(outfile, data, metadata=f.metadata, resolution=fp.get_resolution(f))

''' 2. REGULARISE STACKS '''
dir_ = '/media/henrik/Seagate Expansion Drive/181113-PIN-GFP-MM-dsRed-Timelapse-24h'
files = listdir(dir_, include=['.lsm', '.tif'], sorting='natural')
files = files[:2]

regdir = os.path.join(outdir, 'regularised')
mkdir(regdir)
Parallel(n_jobs=2, verbose=True)(delayed(regularise_stack)(fname=ff, outdir=regdir) for ff in files) 

''' 3. UNSTRETCH '''
stretchdir = os.path.join(outdir, 'regularised')
mkdir(stretchdir)

def grouper(n, iterable, fillvalue=None):
    from itertools import izip_longest
    "grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return izip_longest(fillvalue=fillvalue, *args)

files = listdir(regdir, include=['.lsm', '.tif'], sorting='natural')
pairs = list(grouper(2, files))

# TODO: Parallelise
for ii, pair in enumerate(pairs):
    align_pair(bigpath=pair[1], smallpath=pair[0], outdir=stretchdir, background_threshold=[1500, 1500], autocrop_threshold=[10000, 5000], 
               autocrop_n=[10, 10], score_threshold=.8, weight='relative_intensity', 
               measure='correlation')

files = listdir(regdir, include=['.lsm', '.tif'], sorting='natural', exclude='5um')

def destretch(fname, outdir=''):
    import pandas as pd
    data = pd.read_csv(stretchdir+ '/stretching.txt', header=0, sep='\t')
    row = data.loc[data['fname'] == os.path.splitext(os.path.basename(fname))[0]]
    stretch = row['mean_stretch'][0]
    if stretch < 1.0:
        stretch = 1.0
    f = fp.tiffload(fname)
    f.metadata['voxelsizez'] = f.metadata['voxelsizez'] / stretch
    f.metadata['spacing'] = f.metadata['spacing'] / stretch
    fp.tiffsave(outdir + '/' + os.path.splitext(os.path.basename(fname))[0] + '_destretched.tif', 
                f.data, f.metadata, resolution=[f.metadata['voxelsizez'], 
                                                f.metadata['voxelsizey'], 
                                                f.metadata['voxelsizex']])
destretchdir = regdir + '/../destretch'
mkdir(destretchdir)

for fname in files:
    destretch(fname, outdir=destretchdir)

files = listdir(destretchdir, include=['.lsm', '.tif'], sorting='natural', exclude='5um')
deconvdir = destretchdir + '/../deconvolved'
mkdir(deconvdir)

deconvolve_file(fname, outdir=deconvdir, background_threshold=[1500, 1500], autocrop_threshold=7000, 
                autocrop_n=10, iterations=[2, 20], magnification=20, NA=1, ex_wavelens=[488, 514], 
                em_wavelens=[np.mean([489.2, 522.82]), np.mean([565.51, 615.98])], 
                pinhole_radii=[29.1096 / 2. / 20] * 2, pinhole_shape='square')


''' Max projections '''
maxproj_dir = os.path.join(outdir, 'projections/')

files = listdir(outdir, include=['.tif', '.lsm'], sorting='natural')
p = re.compile(".._npa-\d")
plants = []
for ii in files:
  for jj in p.finditer(ii):
    plants.append(jj.group())
plants = np.unique(plants)

for plant in plants:
    plantfiles = np.array([ff for ff in files if plant in ff])
    intensity_projection_series_all(plantfiles, maxproj_dir + plant + '_max_projection_norm_all.tif',
                                    fct=np.max, normalize='all')
    intensity_projection_series_all(plantfiles, maxproj_dir + plant + '_max_projection_norm_first.tif',
                                    fct=np.max, normalize='first')
    intensity_projection_series_all(plantfiles, maxproj_dir + plant + '_max_projection_norm_none.tif',
                                    fct=np.max, normalize=None)



    