#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 11:02:24 2018

@author: henrik
"""

import os
import phenotastic.file_processing as fp
from pystackreg import StackReg
from skimage.transform import warp
import numpy as np
import gc
from stackAlign.alignment import z_correction

OUTPUT_PATH    = '/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h_STACKREG_STRETCH/'
STRETCH_FILE   = os.path.join(OUTPUT_PATH, 'stretching.txt')
ALIGNMENT_FILE = os.path.join(OUTPUT_PATH, 'alignment.txt')

z_correction('/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h_STACKREG/',
             STRETCH_FILE, rapidId='-5um', refId='reference',
             method='rigid',
             gaussian_filter_val=0,
             pruneBottom=2,
             pruneTop=4,
             printAlignment=True, p=None)
