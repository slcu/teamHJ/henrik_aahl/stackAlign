#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  6 16:42:34 2018

@author: henrik
"""

import phenotastic.file_processing as fp
from pystackreg import StackReg
from tifffile import imshow
from skimage.transform import warp
import numpy as np


''' off 3 '''
p1 = '/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h/pWUS-3XVENUS-pCLV3-mCherry-off_npa-3-48h_part1.lsm'
p2 = '/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h/pWUS-3XVENUS-pCLV3-mCherry-off_npa-3-48h_part2.lsm'
f1 = fp.tiffload(p1)
f2 = fp.tiffload(p2)
f1.data = f1.data[:215]
metadata = f1.metadata

new = np.concatenate([f1.data, f2.data])
new_mean = np.mean(new, axis=1)

sr = StackReg(StackReg.RIGID_BODY)
tmats = sr.register_stack(new_mean, reference='previous')

for ii in xrange(new.shape[0]):
    for jj in xrange(new.shape[1]):
        new[ii, jj] = warp(new[ii, jj], tmats[ii], preserve_range=True)

fp.tiffsave('/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h_STACKREG/pWUS-3XVENUS-pCLV3-mCherry-off_npa-3-48h.tif', new,
            resolution=[metadata['voxelsizez'],
                        metadata['voxelsizey'],
                        metadata['voxelsizex']],
                        metadata=metadata)

''' off 5 '''
p1 = '/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h/pWUS-3XVENUS-pCLV3-mCherry-off_npa-5-48h_part1.lsm'
p2 = '/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h/pWUS-3XVENUS-pCLV3-mCherry-off_npa-5-48h_part2.lsm'

f1 = fp.tiffload(p1)
f2 = fp.tiffload(p2)
metadata = f1.metadata
f2.data = f2.data[42:]
f1.data = f1.data[:303]

new = np.concatenate([f1.data, f2.data])
new_mean = np.mean(new, axis=1)

sr = StackReg(StackReg.RIGID_BODY)
tmats = sr.register_stack(new_mean, reference='previous')

for ii in xrange(new.shape[0]):
    for jj in xrange(new.shape[1]):
        new[ii, jj] = warp(new[ii, jj], tmats[ii], preserve_range=True)

fp.tiffsave('/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h_STACKREG/pWUS-3XVENUS-pCLV3-mCherry-off_npa-5-48h.tif', new,
            resolution=[metadata['voxelsizez'],
                        metadata['voxelsizey'],
                        metadata['voxelsizex']],
                        metadata=metadata)
''' on 5 '''
p1 = '/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h/pWUS-3XVENUS-pCLV3-mCherry-on_npa-5-72h_part1.lsm'
p2 = '/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h/pWUS-3XVENUS-pCLV3-mCherry-on_npa-5-72h_part2.lsm'

f1 = fp.tiffload(p1)
metadata = f1.metadata
f2 = fp.tiffload(p2)
f1.data = f1.data[:197]
#f2.data = f2.data[42:]

new = np.concatenate([f1.data, f2.data])
new_mean = np.mean(new, axis=1)

sr = StackReg(StackReg.RIGID_BODY)
tmats = sr.register_stack(new_mean, reference='previous')

for ii in xrange(new.shape[0]):
    for jj in xrange(new.shape[1]):
        new[ii, jj] = warp(new[ii, jj], tmats[ii], preserve_range=True)

fp.tiffsave('/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h_STACKREG/pWUS-3XVENUS-pCLV3-mCherry-on-npa-5-72h.tif', new,
            resolution=[metadata['voxelsizez'],
                        metadata['voxelsizey'],
                        metadata['voxelsizex']],
                        metadata=metadata)
f = fp.tiffload('/home/henrik/data/180801-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-24h_STACKREG/pWUS-3XVENUS-pCLV3-mCherry-on-npa-5-72h.tif')

