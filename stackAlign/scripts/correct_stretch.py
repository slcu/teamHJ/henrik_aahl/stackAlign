#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 21 14:37:33 2018

@author: henrik
"""
import functools
import argparse
import os
import multiprocessing
from stackAlign.misc import mkdir
import numpy as np
import stackAlign.imageProc as ip
import subprocess
import stackAlign.background_extraction as bg
import tifffile as tiff
import copy
from stackAlign.alignment import z_correction
import pandas as pd
import phenotastic.file_processing as fp

if_default  = '/home/henrikahl/home3/microscopy/teamhj/Henrik-Aahl/180312-pWUS-3XVENUS-pCLV3-mCherry-Timelapse-6h/off_NPA'
of_default  = os.getcwd() + 'stackAlign_output'
saf_default = '/home/henrikahl/projects/stackAlign'
rid_default = '-5um'
refid_default = 'reference'
r_default   = [515, 545, 580, 633, 654, 735]
di_default  = [10, 8, 3]
#exit()
parser = argparse.ArgumentParser(description='')
parser.add_argument('input_folder',        type=str, default=if_default)
parser.add_argument('output_folder',       type=str, default=of_default)
parser.add_argument('--stackalign_folder', type=str, default=saf_default)
parser.add_argument('--rapid_id',          type=str, default=rid_default)
parser.add_argument('--reference_id',      type=str, default=refid_default)
parser.add_argument('--nproc',             type=int, default=1)

args = parser.parse_args()


################################################################################
INPUT_PATH      = args.input_folder
OUTPUT_PATH     = args.output_folder
#STACKALIGN_PATH = args.stackalign_folder
RAPID_ID        = args.rapid_id
NPROCESSORS     = args.nproc
REFERENCE_ID    = args.reference_id
STACKALIGN_PATH = os.path.abspath('')

RAPID_ID = '-5um'
REFERENCE_ID = 'reference'

ALIGNMENT_PRUNE_BOTTOM = 2
ALIGNMENT_PRUNE_TOP = 4
ALIGNMENT_GAUSSIAN_FILTER = 0
ALIGNMENT_METHOD = 'rigid'

''' DIRECTORIES AND FILES '''
STRETCH_FILE    = os.path.join(OUTPUT_PATH, 'stretching.txt')
ALIGNMENT_FILE  = os.path.join(OUTPUT_PATH, 'alignment.txt')
TIFFS_PATH      = os.path.join(OUTPUT_PATH, 'tiffs')
CROP_PATH       = os.path.join(OUTPUT_PATH, 'cropped')
DECONV_PATH     = os.path.join(OUTPUT_PATH, 'deconv')
FILTERED_PATH   = os.path.join(OUTPUT_PATH, 'filtered')
MIPS_PATH       = os.path.join(OUTPUT_PATH, 'mips')
PSF_PATH        = os.path.join(OUTPUT_PATH, 'PSFs')
PSF_CONFIG_PATH = os.path.join(PSF_PATH,    'configs')

''' SOFTWARE '''
################################################################################
##############################  RUN STUFF  #####################################
################################################################################
''' Create output directories '''
print('Creating output directories...')
mkdir(OUTPUT_PATH)
mkdir(DECONV_PATH)

for ii in xrange((3)):
    mkdir(os.path.join(OUTPUT_PATH, 'C%d' % (ii + 1)))

################################################################################
'''' Converting to tiffs '''
p = multiprocessing.Pool(NPROCESSORS)

################################################################################

''' Correct for stretching '''
z_correction(INPUT_PATH, STRETCH_FILE, rapidId=RAPID_ID, refId=REFERENCE_ID,
             method=ALIGNMENT_METHOD,
             gaussian_filter_val=ALIGNMENT_GAUSSIAN_FILTER,
             pruneBottom=ALIGNMENT_PRUNE_BOTTOM,
             pruneTop=ALIGNMENT_PRUNE_TOP,
             printAlignment=True,
             p=p)

#def correct_stretch(fname, stretch_file=STRETCH_FILE, var='meanStretch',
#                    allow_compression=False):
#    fnamebase = os.path.splitext(os.path.basename(fname))[0]
#
#    # Read stretch file
#    data = pd.read_table(STRETCH_FILE, skiprows=11, sep = "\t",
#                         names=['file', 'meanStretch', 'medianStretch', 'modeStretch',
#                                'stdStretch', 'time', 'pairs', 'scores'])
#
#    try:
#      stretch = data[(data['file'] == fnamebase).values][var].values[0]
#    except (KeyError, IndexError):
#        print('''Error: File {fnamebase} could not be found. Setting stretch to
#              1.0.'''.format(fnamebase=fnamebase))
#        stretch = 1.0
#
#    if not allow_compression and stretch < 1.0:
#      stretch = 1.0
#
#    # Correct metadata
#    file_ = fp.tiffload(fname)
#    meta = file_.metadata
#    meta['spacing'] /= stretch
#    #voxelsize[2] /= stretch
#    if 'voxelsizez' in meta:
#        meta['voxelsizez'] /= stretch
#
#    resolution = [meta['spacing'], 1, 1]
#
#    if 'voxelsizey' in meta:
#        resolution[1] = meta['voxelsizey']
#    if 'voxelsizex' in meta:
#        resolution[2] = meta['voxelsizex']
#
#    fp.tiffsave(fname, file_.data, resolution=resolution, metadata=meta)
#
#print('Correcting for stretching...')
#
#def listdir(path, include=None, exclude=None, full=True):
#    files = os.listdir(path)
#
#    if full:
#        files = map(lambda x: os.path.join(path, x), files)
#
#    # TODO
#    if include is not None:
#        files = filter(lambda x: np.any(np.in1d(include, x)), files)
#    if exclude is not None:
#        files = filter(lambda x: not np.all(np.in1d(exclude, x)), files)
#
#    return files
#
#
#sfiles = filter(lambda x: RAPID_ID not in x, cfiles)
#p.map(functools.partial(correct_stretch, stretch_file=STRETCH_FILE,
#                        var='meanStretch'), sfiles)

